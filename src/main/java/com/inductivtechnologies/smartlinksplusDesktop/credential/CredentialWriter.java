package com.inductivtechnologies.smartlinksplusDesktop.credential;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Util;

/**
 * CredentialWriter : Permet d'écrire les informations d'authentification et token 
 */
public class CredentialWriter {
	
	public static final String TAG = CredentialWriter.class.getSimpleName();
	
	//Crédentials
	private static final String USERNAME = "InductivTech";
	private static final String PASSWORD = "AdminInductivTech2018++SmarLPL";
	
	private static final String[] CREDENTIAL_COLUMNS = {"_id", "Value", "ExpirationDate", "CreationDate"};
	
	/**
     * Ecrit les informations dans le fichier CredentialToken 
     * @param token Le token à écrire 
     */
	public static boolean writeCredentialInfos(Token token) {
		File fileToken = new File(Util.credentialFile);
		
		if(fileToken.exists()) {
			writeToken(fileToken, token);
			return true;
		}else {
			if(Util.generateApplicationTree()) {
				writeToken(fileToken, token);
				return true;
			}
		}
		
		return false;
	}
	
	/**
     * Ecrit le token dans le fichier CredentialToken 
     * @param token Le token à écrire 
     * @param file Le fichie ou lire 
     */
	private static void writeToken(File file, Token token) {
		FileWriter fileWriter;
		
		WhatIDo.message(TAG, "Sauvegarde de la clé d'accès");
		
		try {
			fileWriter = new FileWriter(file);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			
			//Le contenu du fichier 
			StringBuilder fileContent = new StringBuilder();
			//Le contenu des colonnes 
			StringBuilder strColumns = new StringBuilder();
			
			//On construit les colonnes 
			for(int i= 0; i < CREDENTIAL_COLUMNS.length; i++){
				strColumns.append(CREDENTIAL_COLUMNS[i]);
				//Le séparateur 
				if(i != (CREDENTIAL_COLUMNS.length - 1)) strColumns.append(",");
			}
			
			//On ajoute les colonnes 
			fileContent.append(strColumns);
			fileContent.append("\n");
			
			//On construit les données  
			//On ajoute une ligne de données
			fileContent.append(Util.transformTokenToString(token));
			//Le retour à la ligne 
			fileContent.append("\n");
			
			printWriter.write(fileContent.toString());
			printWriter.close();
			WhatIDo.message(TAG, "Sauvegarde de la clé d'accès terminée");
			
		}catch(IOException exception) {
			WhatIDo.message(TAG, "Sauvegarde de la clé d'accès non aboutie");
	        System.out.println(exception);
		}
	}

	public static String getUsername() {
		return USERNAME;
	}

	public static String getPassword() {
		return PASSWORD;
	}
	
}
