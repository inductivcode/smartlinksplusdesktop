package com.inductivtechnologies.smartlinksplusDesktop.credential;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.readers.CSVSplitter;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Util;

/**
 * CredentialReader : Permet d'acceder aux informations d'authentification et token 
 */
public class CredentialReader {
	
	public static final String TAG = CredentialReader.class.getSimpleName();
	
	/**
	 * csvSplitter : Pour décomposer l csves lignes du fichier
	 */
	private static final CSVSplitter csvSplitter = new CSVSplitter(',', '"', true, true);
	
	/**
     * Fournit les informations sur le token 
     * @return Le token 
     */
	public static Token readCredentialInfos(){
		File fileToken = new File(Util.credentialFile);
		
		if(fileToken.exists()) return Util.buildToken(readToken(fileToken));
		return null;
	}
	
	/**
     * Lit le token contenu dans le fichier CredentialToken 
     * @return token Le token à écrire 
     * @return Un tableau contenant les valeurs du token 
     */
	private static String[] readToken(File file) {
		String[] tokenValues = null;
		
		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		
		try{
			fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			
			if(bufferedReader.ready()){
				//Première ligne 
				String firstLine = bufferedReader.readLine();
				//Seconde ligne
				String nextLine = bufferedReader.readLine();
				
				if(!firstLine.isEmpty() && !nextLine.isEmpty()){
					tokenValues = csvSplitter.split(nextLine);
				}
			}
		}catch(IOException exception) {
			exception.printStackTrace();
		}finally{
			try{
				if(bufferedReader != null)
					bufferedReader.close();

				if(fileReader != null)
					fileReader.close();
			}catch(IOException exception) {
		        System.out.println(exception);
			}
		}
		
		return tokenValues;
	}
	
}
