package com.inductivtechnologies.smartlinksplusDesktop.controllers.account;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.TokenCallback;
import com.inductivtechnologies.smartlinksplusDesktop.credential.CredentialWriter;
import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusDesktop.services.account.TokenService;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildRetrofit;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildServiceInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * TokenController : Permet d'invoquer et de gérer les appels du service << TokenService >>
 */
public class TokenController {
	
	public static final String TAG = TokenController.class.getSimpleName();
	
	/**
	 * Parseur GSON 
	 */
	private Gson gsonConverter;
	
	/**
	 * Retrofit
	 */
	private Retrofit retrofit;
	
	/**
	 * Le service
	 */
	private TokenService tokenService;

	/**
	 * Constructeur
	 */
	public TokenController(){
		super();
		this.gsonConverter = BuildGson.buildSimpleGson();
		this.retrofit = BuildRetrofit.simpleRetrofitBuilder(this.gsonConverter).build();
		this.tokenService = BuildServiceInstance.tokenServiceInstance(this.retrofit);
	}
	
	/**
	 * Valide un token 
	*/
	public void checkValidityToken(final TokenCallback tokenCallback, final Token token){
	
		Callback<ApiResponse> callback = new Callback<ApiResponse>() {

			@Override
			public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response){
				tokenCallback.displayNotification("Vérification de l'accès terminée.");
				WhatIDo.message(TAG, "Vérification de l'accès terminée.");
				
				boolean result = false;
				String validToken = null;
				
				if(response.isSuccessful()) {
					ApiResponse apiResponse = response.body();
					
					switch(apiResponse.getCode()){
						case ApiResponse.CODE_SUCCESS :
							tokenCallback.displayNotification("Accès accordé.");
							WhatIDo.message(TAG, "Accès accordé.");
							
							//Résultat 
							result = true;
							validToken = token.getValue();
							break;
						case ApiResponse.CODE_TOKEN_EXPIRED : 
							tokenCallback.displayNotification("Token expiré et regénéré.");
							
							String jsonString = BuildGson.buildPrettyGson().toJson(apiResponse
	  								.getData()
	  								.get(Token.TOKEN_DATA_NAME));
							try {
								JSONObject jsonobject = new JSONObject(jsonString);
								Token newToken = new Token(jsonobject);
								
								//Résultat 
								result = CredentialWriter.writeCredentialInfos(newToken);
								validToken = newToken.getValue();
							}catch(JSONException e){
								tokenCallback.onFailure("Vérification de l'accès non aboutie.Réessayez.");
								WhatIDo.message(TAG, "Vérification de l'accès non aboutie.Réessayez.");
								
								e.printStackTrace();
							}
							break;
						default: 
							tokenCallback.onFailure(apiResponse.getMessage());
							WhatIDo.message(TAG, apiResponse.getMessage());
							break;
					}
				}else{
					tokenCallback.onFailure("Vérification de l'accès non aboutie.Réessayez.");
					WhatIDo.message(TAG, "Vérification de l'accès non aboutie.Réessayez.");
				}
				
				tokenCallback.checkValidityToken(result, validToken);
			}
			
			@Override
			public void onFailure(Call<ApiResponse> call, Throwable t){
				tokenCallback.onFailure("Vérification de l'accès non aboutie.Vérifiez votre connexion.");
				WhatIDo.message(TAG, "Vérification de l'accès non aboutie.Vérifiez votre connexion.");
				
				t.printStackTrace();
			}
			  
		};
		
		Call<ApiResponse> callService = tokenService.checkToken(token.getValue());
		callService.enqueue(callback);
		
		tokenCallback.displayNotification("Vérification de l'accès ...");
		WhatIDo.message(TAG, "Vérification de l'accès ...");
	}
	
}
