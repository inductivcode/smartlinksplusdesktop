package com.inductivtechnologies.smartlinksplusDesktop.controllers.account;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.AuthCallback;
import com.inductivtechnologies.smartlinksplusDesktop.credential.CredentialWriter;
import com.inductivtechnologies.smartlinksplusDesktop.models.account.Credential;
import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusDesktop.services.account.UserService;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildRetrofit;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildServiceInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * UserController : Permet d'invoquer et de gérer les appels du service << UserService >>
 */
public class UserController {
	
	public static final String TAG = UserController.class.getSimpleName();
	
	/**
	 * Constructeur
	 */
	public UserController() {
		super();
	}
	
	/**
	 * authUser : Authentifie un utilisateur à partir de 
	 * son nom utilisateur et son mot de passe 
	 * @param credential Les informations authentification
	 * @param authCallback Un callback pour suivre les états de la requête
	 */
	public void authUser(final AuthCallback authCallback, Credential credential){
		//L'objet Gson 
		Gson gsonConverter = BuildGson.buildSimpleGson();
				
		//Le retrofit 
		Retrofit retrofit = BuildRetrofit
				.simpleRetrofitBuilder(gsonConverter)
				.build();
				
		UserService userService = BuildServiceInstance
				.UserServiceInstance(retrofit);
		
		Callback<ApiResponse> callback = new Callback<ApiResponse>() {

			@Override
			public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response){
				
				Token token = null;
				
				if(response.isSuccessful()) {
					ApiResponse apiResponse = response.body();

					switch(apiResponse.getCode()) {
						case ApiResponse.CODE_SUCCESS : 
							authCallback.displayNotification(apiResponse.getMessage());
							
							String jsonString = BuildGson.buildPrettyGson().toJson(apiResponse
									.getData()
	  								.get(Token.TOKEN_DATA_NAME));
							try{
								JSONObject jsonobject = new JSONObject(jsonString);
								token = new Token(jsonobject);
								//On écrit la clé d'accès
								CredentialWriter.writeCredentialInfos(token);
							}catch(JSONException e) {
								authCallback.onFailure("Authentification non aboutie.Rééssayez.");
								WhatIDo.message(TAG, "Authentification non aboutie.Rééssayez.");
								
								e.printStackTrace();
							}
							break;
						default :
							authCallback.onFailure(apiResponse.getMessage());
							WhatIDo.message(TAG, "Error : " + apiResponse.getMessage());
							break;
					}
				}else{
					authCallback.onFailure("Authentification non aboutie.Rééssayez.");
					WhatIDo.message(TAG, "Authentification non aboutie.Rééssayez.");
				}
				
				authCallback.authUser(token);
			}

			@Override
			public void onFailure(Call<ApiResponse> call, Throwable t){
				authCallback.onFailure("Authentification non aboutie.Vérifiez votre connexion.");
				WhatIDo.message(TAG, "Authentification non aboutie.Vérifiez votre connexion.");
				
				t.printStackTrace();
			}
			
		};
				
		Call<ApiResponse> callService = userService.authenticateUser(credential);
		callService.enqueue(callback);
		
		authCallback.displayNotification("Authentification en cours ...");
		WhatIDo.message(TAG, "Authentification en cours ...");
	}

}
