package com.inductivtechnologies.smartlinksplusDesktop.controllers.market;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.HttpCallback;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.RawMarketDataService;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Constants;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.DisplaySampleData;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildRetrofit;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildServiceInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * RawMarketDataController : Permet d'invoquer et de gérer les appels 
 * du service << RawMarketDataService >>
 */
public class RawMarketDataController {
	
	public static final String TAG = RawMarketDataController.class.getSimpleName();
	
	/**
	 * Parseur GSON 
	 */
	private Gson gsonConverter;
	
	/**
	 * Retrofit
	 */
	private Retrofit retrofit;
	
	/**
	 * Le service
	 */
	private RawMarketDataService rawMarketDataService;
	
	/**
	 * Constructeur
	 * @param frame La vue 
	 */
	public RawMarketDataController(String tokenValue) {
		super();
		this.gsonConverter = BuildGson.buildSimpleGson();
		this.retrofit = BuildRetrofit
				.authTokenRetrofitBuilder(this.gsonConverter, tokenValue).build();
		this.rawMarketDataService = BuildServiceInstance.rawMarketDataServiceInstance(this.retrofit);
	}
	

	/**
	 * Récupère des marchés entre deux dates 
	 * @param symbol Le symbole 
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	*/
	public void rawMarketDataBetweenTwoDate(final HttpCallback httpCallback,
		final String marketType, final String symbol, 
		final String startDate,
		final String endDate){
		
		Callback<ApiResponse> callback = new Callback<ApiResponse>(){
			@Override
			public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response){
				WhatIDo.message(TAG, "Chargement terminé.");
				
				if(response.isSuccessful()) {
					ApiResponse apiResponse = response.body();
					
					switch(apiResponse.getCode()){
						case ApiResponse.CODE_SUCCESS :
							String jsonString = BuildGson.buildPrettyGson().toJson(apiResponse
										.getData()
										.get(marketType.toLowerCase()));
								
							JSONArray jsonArray;
							List<RawMarketData> markets;
							try {
								jsonArray = new JSONArray(jsonString);
								markets = EntitiesUtil.getListRawMarketDataByJSONArray(jsonArray);
								
								//Pour le fun 
		  						if(markets != null) {
		  						    //On trnsmet à la vue 
		  							switch(marketType){
			  							case Constants.EQUITY_TYPE :
			  								httpCallback.equitiesBetweenTwoDate(markets, symbol);
			  								break;
			  							case Constants.FXSPOT_TYPE :
			  								httpCallback.fxSpotBetweenTwoDate(markets, symbol);
			  								break;
			  					    }
		  							
		  							WhatIDo.message(TAG, "Résultat : " + markets.size() + " Trouvés");
		  							DisplaySampleData.viewMarkets(markets);
		  					    }else{
		  					    	httpCallback.onFailure("Erreur inattendue.Rééssayez.");
		  					    }
							}catch(JSONException e) {
								//Notification
								httpCallback.onFailure("Erreur inattendue.Rééssayez.");
								
								WhatIDo.message(TAG, "Chargement non abouti.Rééssayez.");
								e.printStackTrace();
							}
							break;
						default: 
	  						//Notification
							httpCallback.onFailure(apiResponse.getMessage());
							
	  						WhatIDo.message(TAG, apiResponse.getMessage());
	  						break;
					}
				}
			}

			@Override
			public void onFailure(Call<ApiResponse> call, Throwable t){
				//Notification
				httpCallback.onFailure("Erreur inattendue.Vérifiez votre connexion.");
				
				WhatIDo.message(TAG, "Chargement non abouti.Vérifiez votre connexion.");
				t.printStackTrace();
			}
	    	
	    };
	    
	    Call<ApiResponse> callService = null;
	    switch(marketType){
			case Constants.EQUITY_TYPE :
				callService = rawMarketDataService
						.equitiesBetweenTwoDate(symbol, startDate, endDate);
				break;
			case Constants.FXSPOT_TYPE :
				callService = rawMarketDataService
						.fxspotBetweenTwoDate(symbol, startDate, endDate);
				break;
	    }
	    callService.enqueue(callback);
	    
		httpCallback.displayNotification("Chargement pour " + symbol + " entre " 
				+ startDate + " et " + endDate);
    	WhatIDo.message(TAG, "Chargement des equities.");
	}
}
