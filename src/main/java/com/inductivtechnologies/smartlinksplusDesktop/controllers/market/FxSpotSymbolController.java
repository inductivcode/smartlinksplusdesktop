package com.inductivtechnologies.smartlinksplusDesktop.controllers.market;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.HttpCallback;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.FxSpotSymbolService;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.DisplaySampleData;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildRetrofit;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildServiceInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * FxSpotSymbolController : Permet d'invoquer et de gérer les appels du service << FxSpotSymbolService >>
 */
public class FxSpotSymbolController{
	
	public static final String TAG = FxSpotSymbolController.class.getSimpleName();
	
	/**
	 * Parseur GSON 
	 */
	private Gson gsonConverter;
	
	/**
	 * Retrofit
	 */
	private Retrofit retrofit;
	
	/**
	 * Le service
	 */
	private FxSpotSymbolService fxSpotSymbolService;
	
	/**
	 * Constructeur
	 */
	public FxSpotSymbolController(String tokenValue) {
		super();
		this.gsonConverter = BuildGson.buildSimpleGson();
		this.retrofit = BuildRetrofit
				.authTokenRetrofitBuilder(this.gsonConverter, tokenValue).build();
		this.fxSpotSymbolService = BuildServiceInstance.fxspotSymbolServiceInstance(this.retrofit);
	}
	
	/**
	 * Retourne tous les symbols fx spot
	*/
	public void allFxSpotSymbols(final HttpCallback httpCallback) {
		
		  Callback<ApiResponse> callback = new Callback<ApiResponse>() {
				@Override
				public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response){
					WhatIDo.message(TAG, "Chargement terminé.");
					
					if(response.isSuccessful()) {
						ApiResponse apiResponse = response.body();
						
						switch(apiResponse.getCode()){
							case ApiResponse.CODE_SUCCESS :
								String jsonString = BuildGson.buildPrettyGson().toJson(apiResponse
										.getData()
										.get(FxSpotSymbol.FXSPOTSYMBOL_DATA_NAME.toLowerCase()));
								
								JSONArray jsonArray;
								List<FxSpotSymbol> symbols;
								try {
									jsonArray = new JSONArray(jsonString);
									symbols = EntitiesUtil.getListFxSpotSymbolByJSONArray(jsonArray);
									
									//Pour le fun 
			  						if(symbols != null) {
			  						    //On trnsmet à la vue 
			  							httpCallback.allFxSpotSymbols(symbols);
			  							
			  							WhatIDo.message(TAG, "Résultat : " + symbols.size() + " Trouvés");
			  							DisplaySampleData.viewFxSpotSymbols(symbols);
			  					    }else{
			  					    	httpCallback.onFailure("Erreur inattendue.Rééssayez.");
			  					    }
								}catch(JSONException e) {
									//Notification
									httpCallback.onFailure("Erreur inattendue.Rééssayez.");
									
									WhatIDo.message(TAG, "Erreur inattendue.Rééssayez.");
									e.printStackTrace();
								}
		  						break;
							default: 
		  						//Notification
								httpCallback.onFailure(apiResponse.getMessage());
								
		  						WhatIDo.message(TAG, apiResponse.getMessage());
		  						break;
			  			}
					}
				}

				@Override
				public void onFailure(Call<ApiResponse> call, Throwable t){
					//Notification
					httpCallback.onFailure("Erreur inattendue.Vérifiez votre connexion.");
					
					WhatIDo.message(TAG, "Chargement non abouti.Vérifiez votre connexion.");
					t.printStackTrace();
				}
		    	
		};
	    
		Call<ApiResponse> callService = fxSpotSymbolService.allFxSpotSymbols();
		callService.enqueue(callback);
    	
		httpCallback.displayNotification("Chargement des symboles en cours ...");
    	WhatIDo.message(TAG, "Chargement des symboles FXSPOT.");
	}
}
