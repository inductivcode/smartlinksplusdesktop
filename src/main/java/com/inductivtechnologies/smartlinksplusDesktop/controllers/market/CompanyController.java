package com.inductivtechnologies.smartlinksplusDesktop.controllers.market;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.HttpCallback;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.Company;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.CompanyService;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.DisplaySampleData;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildRetrofit;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildServiceInstance;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * FxSpotSymbolController : Permet d'invoquer et de gérer les appels du service << CompanyService >>
 */
public class CompanyController {
	
	public static final String TAG = CompanyController.class.getSimpleName();

	/**
	 * Parseur GSON 
	 */
	private Gson gsonConverter;
	
	/**
	 * Retrofit
	 */
	private Retrofit retrofit;
	
	/**
	 * Le service
	 */
	private CompanyService companyService;
	
	/**
	 * Constructeur
	 */
	public CompanyController(String tokenValue) {
		super();
		this.gsonConverter = BuildGson.buildSimpleGson();
		this.retrofit = BuildRetrofit
				.authTokenRetrofitBuilder(this.gsonConverter, tokenValue).build();
		this.companyService = BuildServiceInstance.companyServiceInstance(this.retrofit);
	}
	
	/**
	 * Récupère toutes les compagnies
	*/
	public void allCompanies(final HttpCallback httpCallback){
	    
	    Callback<ApiResponse> callback = new Callback<ApiResponse>() {
			@Override
			public void onResponse(Call<ApiResponse> call, Response<ApiResponse> response){
				WhatIDo.message(TAG, "Chargement des compagnies terminé.");
				
				if(response.isSuccessful()) {
	  				ApiResponse apiResponse = response.body();
	  				
	  				switch(apiResponse.getCode()){
	  					case ApiResponse.CODE_SUCCESS :
	  						String jsonString = BuildGson.buildPrettyGson().toJson(apiResponse
	  								.getData()
	  								.get(Company.COMPANY_DATA_NAME_PLU));
	  						
							JSONArray jsonArray;
							List<Company> companies;
							try {
								jsonArray = new JSONArray(jsonString);
								companies = EntitiesUtil.getListCompaniesByJSONArray(jsonArray);
								
								//Pour le fun 
		  						if(companies != null) {
		  						    //On trnsmet à la vue 
		  							httpCallback.allCompanies(companies);
		  							
		  							WhatIDo.message(TAG, "Résultat : " + companies.size() + " Trouvés");
		  							DisplaySampleData.viewCompanies(companies);
		  					    }else{
		  					    	httpCallback.onFailure("Erreur inattendue.Rééssayez.");
		  					    }
							}catch(JSONException e){
								//Notification
								httpCallback.onFailure("Erreur inattendue.Rééssayez.");
								
								WhatIDo.message(TAG, "Chargement non abouti.");
								e.printStackTrace();
							}
	  						break;
	  					default: 
	  						//Notification
	  						httpCallback.onFailure(apiResponse.getMessage());
	  						
	  						WhatIDo.message(TAG, apiResponse.getMessage());
	  						break;
	  				}
	  			}
			}

			@Override
			public void onFailure(Call<ApiResponse> call, Throwable t){
				//Notification
				httpCallback.onFailure("Erreur inattendue.Vérifier votre connexion.");
				
				WhatIDo.message(TAG, "Chargement non abouti.Vérifier votre connexion.");
				t.printStackTrace();
			}
	    	
	    };
	    
	    Call<ApiResponse> callService = companyService.allCompanies();
    	callService.enqueue(callback);
    	
    	httpCallback.displayNotification("Chargement des compagnies en cours ...");
    	WhatIDo.message(TAG, "Chargement des compagnies.");
	}
}
