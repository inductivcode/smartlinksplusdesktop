package com.inductivtechnologies.smartlinksplusDesktop.writers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Util;

/**
 * DataWriter : Regroupe les méthodes qui permettent d'écrire
 *  des données distantes (HTTP) dans des fichiers CSV
 */
public class DataWriter {
	
	public static final String TAG = DataWriter.class.getSimpleName();
	
	/**
     * Ecrit des données dans un fichier
     * @param fileName Le nom du fichier 
     * @param content Le contenu à ecrire
     */
	public static void writeIntoAFile(String fileName, String content){
		FileWriter fileWriter;
		
		try{
			fileWriter = new FileWriter(fileName);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			
			WhatIDo.message(TAG, "Ecriture du fichier patientez ...");
			printWriter.write(content);
			printWriter.close();
	    	WhatIDo.message(TAG, "Ecriture terminée");
	        
		}catch(IOException exception){
		    WhatIDo.message(TAG, "Ecriture non aboutie");
			exception.printStackTrace();
		}
	}
	
	/**
     * Exporte les données contenues dans un JTable vers un fchier CSV
     * @param filename Le nom du fichier 
     * @param data Le contenu à exporter
     * @param columns Les titres des colonnes du fichier
     * @return File Le fichier résultant 
     */
	 public static File exportDataFromJTable(String filename, Object[][] data, String[] columns){
	        boolean createFileResult = Util.createFileIfNotExist(filename);

	        //Le fichier
	        File file = null;
	        FileWriter fileWriter;

	        if(createFileResult){
	        	WhatIDo.message(TAG, "Fichier Demandé : " + filename);
	            file = new File(filename);
	            try {
	                fileWriter = new FileWriter(file);
	                PrintWriter printWriter = new PrintWriter(fileWriter);

	                //Le contenu du fichier
	                StringBuilder fileContent = new StringBuilder();
	                //Le contenu des colonnes
	                StringBuilder strColumns = new StringBuilder();

	            	WhatIDo.message(TAG, "Ecriture du fichier patientez ...");

	                //On construit les colonnes
	                for(int i= 0; i < columns.length; i++){
	                    strColumns.append(columns[i]);
	                    //Le séparateur
	                    if(i != (columns.length - 1)) strColumns.append(";");
	                }

	                //On ajoute les colonnes
	                fileContent.append(strColumns);
	                fileContent.append("\n");

	                //On construit les données
	                for(int i = 0; i < data.length; i++){
	                    //On ajoute une ligne de données
	                    fileContent.append(Util.transformArrayToCSVString(data[i]));
	                    //Le retour à la ligne
	                    fileContent.append("\n");
	                }

	                printWriter.write(fileContent.toString());
	                printWriter.close();

	            	WhatIDo.message(TAG, "Ecriture terminée");
	            }catch (IOException exception) {
	            	WhatIDo.message(TAG, "Ecriture non aboutie.");
	                System.out.println(exception);
	            }
	        }

	        return file;
	    }
}
