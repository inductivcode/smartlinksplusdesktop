package com.inductivtechnologies.smartlinksplusDesktop.ui;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

import com.inductivtechnologies.smartlinksplusDesktop.ui.components.SmartFrame;

/**
 * Classe d'accueil permettant de lancer notre fenêtre
 * 
 * @author inductiv
 */

public class Home {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException,
	    UnsupportedLookAndFeelException, ClassNotFoundException {

	SmartFrame home = new SmartFrame("Accueil");

	try {

	     UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Exception e) {
	    e.printStackTrace();

	}
	SwingUtilities.updateComponentTreeUI(home);
	home.setResizable(false);
	home.setVisible(true);
	;
    }

}
