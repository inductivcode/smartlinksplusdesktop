package com.inductivtechnologies.smartlinksplusDesktop.ui.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Calendar;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import javax.swing.border.EtchedBorder;

import com.toedter.calendar.JDateChooser;

/**
 * 
 * Notre fenêtre principale
 * 
 * @author inductiv
 *
 */
public class SmartFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    // début de déclaration des components de notre fenêtre

    // déclaration des panels
    private JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));

    private JPanel panelRight = new JPanel(new FlowLayout(FlowLayout.CENTER));
    private JPanel panelLeft = new JPanel();
    private JPanel panelBottom = new JPanel(new FlowLayout(FlowLayout.CENTER));

    private JPanel panellogger = new JPanel(new FlowLayout(FlowLayout.CENTER));

    private JPanel panelCharger = new JPanel(new FlowLayout(FlowLayout.CENTER));

    private JPanel progresspanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    private JSplitPane splitPane; // divise le panel du milieu en deux

    private DataScrollPane dataPanel; // panel scrollable contenant DataTable

    // déclaration des objets contenants nos objets métiers

    private JComboBox<String> listdata;

    private JDateChooser startDate = new JDateChooser();

    private JDateChooser endDate = new JDateChooser();

    public static JTextArea textarealogger = new JTextArea("Details");

    // déclaration du bouton charger qui permet de lancer le download

    private JButton charger = new JButton("Load");

    // récupération de la dimension de l'écran et initialisation des dimensions
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

    private static int width = screenSize.width - 100;

    private static int height = screenSize.height - 100;

    // objets permettant de manipuler les données
    private Calendar startCalendar;

    private Calendar endCalendar;

    private String instrument;

    /**
     * constructeur par défaut de la fenêtre(frame)
     * 
     * @author inductiv
     */

    public SmartFrame() {
    	super();
    }

    /**
     * constructeur de la fenêtre avec paramètre titre de la fenêtre
     * 
     * @param String
     *            title
     * @author inductiv
     */

    public SmartFrame(String title) {

		super(title);
	
		FlowLayout flow = new FlowLayout(FlowLayout.CENTER);
		flow.setAlignOnBaseline(true);
	
		panel.setLayout(flow);
	
		buildLeftPanel();
	
		buildSplitPanel();
	
		buildBottomPanel();
	
		this.setPreferredSize(new Dimension(width, height));
	
		this.setLocationRelativeTo(null);
	
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		this.setExtendedState(JFrame.NORMAL);
	
		setDefaultLookAndFeelDecorated(true);
		panel.add(splitPane);
	
		charger.setPreferredSize(new Dimension(100, 30));
		charger.addActionListener(new EcouteurCharger());
		panelCharger.add(charger);
		panelCharger.setPreferredSize(new Dimension(width, 40));
	
		panel.add(panelCharger);
		panel.add(panelBottom);
		panel.setPreferredSize(new Dimension(width - 5, height - 5));
		this.setContentPane(panel);
	
		this.pack();

    }

    /**
     * méthode qui permet le construire les components du splitPanel
     * 
     * @author inductiv
     */

    public void buildSplitPanel() {

	dataPanel = new DataScrollPane(createDefaultDataTable(), JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	// dataPanel.setBorder(BorderFactory.createEtchedBorder());

	dataPanel.setPreferredSize(new Dimension((int) (width / 2) - 15, (int) height / 2));
	splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, panelLeft, dataPanel);

	splitPane.setPreferredSize(new Dimension(width - 10, (int) height / 2));
	splitPane.setDividerSize(15);
	splitPane.setDividerLocation((int) (width / 2) + 5);

	splitPane.setOneTouchExpandable(true);
	splitPane.setBorder(BorderFactory.createEtchedBorder());

    }

    /**
     * méthode qui permet le construire les components du panel qui se trouve a
     * gauche du splitpane
     * 
     * @author inductiv
     */

    public void buildLeftPanel() {

		FlowLayout fl = new FlowLayout(FlowLayout.CENTER);
	
		panelLeft.setLayout(fl);
	
		String[] st = { "Fxspot", "Equities" };
		listdata = new JComboBox<>(st);
		listdata.addItemListener(new EcouteurData());
		listdata.setPreferredSize(new Dimension((int) width / 4 + 25, 35));
	
		JPanel panelData = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel labelData = new JLabel("Instruments");
		labelData.setPreferredSize(new Dimension(100, 15));
		panelData.add(labelData);
		panelData.add(listdata);
		panelData.setPreferredSize(new Dimension((int) width / 2, 50));
	
		JPanel panelStartDate = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel labelStartDate = new JLabel("Start Date");
		labelStartDate.setPreferredSize(new Dimension(100, 15));
		startDate.setToolTipText("Start Date");
		startDate.setPreferredSize(new Dimension((int) width / 4 + 25, 35));
		startDate.addPropertyChangeListener(new EcouteurStartDate());
		panelStartDate.add(labelStartDate);
		panelStartDate.add(startDate);
		panelStartDate.setPreferredSize(new Dimension((int) width / 2, 50));
	
		JPanel panelEndDate = new JPanel(new FlowLayout(FlowLayout.LEADING));
		JLabel labelEndDate = new JLabel("End Date");
		labelEndDate.setPreferredSize(new Dimension(100, 15));
		endDate.setToolTipText("EndDate Date");
		endDate.setPreferredSize(new Dimension((int) width / 4 + 25, 35));
		endDate.addPropertyChangeListener(new EcouteurEndDate());
		panelEndDate.add(labelEndDate);
		panelEndDate.add(endDate);
		panelEndDate.setPreferredSize(new Dimension((int) width / 2, 50));
	
		panelLeft.add(panelData);
		panelLeft.add(panelStartDate);
		panelLeft.add(panelEndDate);
	
		panelLeft.setMinimumSize(new Dimension((int) (width / 2), (int) height / 2));
		panelLeft.setPreferredSize(new Dimension((int) (width / 2), (int) height / 2));

    }

    /**
     * méthode qui permet le construire les components du panel Bottom
     * 
     * @author inductiv
     */

    public void buildBottomPanel() {

		panellogger.add(textarealogger);
		panellogger.setBackground(Color.WHITE);
	
		panellogger.setPreferredSize(new Dimension(width, 120));
	
	
	
		progresspanel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED, Color.WHITE, Color.WHITE));
		progresspanel.setBackground(Color.WHITE);
		progresspanel.setPreferredSize(new Dimension(width, 80));
	
		panelBottom.setPreferredSize(new Dimension(width, 200));
	
		// panelBottom.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED,Color.WHITE,Color.WHITE));
		panelBottom.setBorder(BorderFactory.createEtchedBorder());
		panelBottom.setBackground(Color.WHITE);
		panelBottom.add(panellogger);
		panelBottom.add(progresspanel);

    }

    /**
     * listener du JButton charger
     * 
     * @author inductiv
     */
    public class EcouteurCharger implements ActionListener {

		public void actionPerformed(ActionEvent e) {
	
		    Calendar startcalendar = getStartCalendar();
		    Calendar endcalendar = getEndCalendar();
		    int yearStart = startcalendar.get(Calendar.YEAR);
		    int monthStart = startcalendar.get(Calendar.MONTH) + 1;
		    int dayStart = startcalendar.get(Calendar.DAY_OF_MONTH);
	
		    DataTable table = (DataTable) dataPanel.getViewport().getView();
		    int[] rows = table.getSelectedRows();
	
		    for (int i = 0; i < rows.length; i++) {
			for (int j = 0; j < table.getColumnCount(); j++) {
			    System.out.println("valeur du tableau a ligne " + i + " et a la colonne " + j + "---"
				    + table.getValueAt(i, j));
			}
		    }
	
		    // System.out.println("-----instrument------"+getInstrument());
	
		}
    }

    /**
     * listener de la date de début implémenté ici par JDateChooser startDate
     * 
     * @author inductiv met a jour le calendar de fin
     */
    public class EcouteurStartDate implements PropertyChangeListener {

	@Override
	public void propertyChange(PropertyChangeEvent evt) {
	    // TODO Auto-generated method stub
	    setStartCalendar(startDate.getCalendar());
	}
    }

    /**
     * listener de la date de fin implémenté ici par JDateChooser endDate
     * 
     * @author inductiv met a jour le calendar de fin
     */

    public class EcouteurEndDate implements PropertyChangeListener {

		@Override
		public void propertyChange(PropertyChangeEvent evt) {
		    // TODO Auto-generated method stub
		    setEndCalendar(endDate.getCalendar());
		}

    }

    /**
     * listener des données(instruments) qui se trouve dans la Jcombobox
     * 
     * @author inductiv
     */

    public class EcouteurData implements ItemListener {

		@Override
		public void itemStateChanged(ItemEvent event) {
		    // TODO Auto-generated method stub
		    if (event.getStateChange() == ItemEvent.SELECTED) {
			Object item = event.getItem();
			setInstrument(item.toString());
		    }
	
		}
    }

    /**
     * methode permettant de créer par défaut une JTable pour le test
     * 
     * @return DataTable
     */
    public DataTable createDefaultDataTable() {

		Vector<Vector<String>> rowData = new Vector<Vector<String>>();
	
		Vector<Vector<String>> columnNames = new Vector<Vector<String>>();
		Vector<String> row = new Vector<String>();
		for (int i = 0; i < 2; i++) {
	
		    row.add("comp ---" + i);
	
		}
		rowData.add(row);
	
		Vector<String> col1 = new Vector<String>();
		Vector<String> col2 = new Vector<String>();
		col1.add("Symbol");
		col2.add("Company");
		columnNames.add(col1);
		columnNames.add(col2);
	
		DataTable compTable = new DataTable(rowData, columnNames);
		compTable.getTableHeader().setReorderingAllowed(false);
		return compTable;
    }

    // Nos setters et getters

    /**
     * @return the panel
     */
    public JPanel getPanel() {
	return panel;
    }

    /**
     * @param panel
     *            the panel to set
     */
    public void setPanel(JPanel panel) {
	this.panel = panel;
    }

    /**
     * @return the panelRight
     */
    public JPanel getPanelRight() {
	return panelRight;
    }

    /**
     * @param panelRight
     *            the panelRight to set
     */
    public void setPanelRight(JPanel panelRight) {
	this.panelRight = panelRight;
    }

    /**
     * @return the panelLeft
     */
    public JPanel getPanelLeft() {
	return panelLeft;
    }

    /**
     * @param panelLeft
     *            the panelLeft to set
     */
    public void setPanelLeft(JPanel panelLeft) {
	this.panelLeft = panelLeft;
    }

    /**
     * @return the panelBottom
     */
    public JPanel getPanelBottom() {
	return panelBottom;
    }

    /**
     * @param panelBottom
     *            the panelBottom to set
     */
    public void setPanelBottom(JPanel panelBottom) {
	this.panelBottom = panelBottom;
    }

    /**
     * @return the panellogger
     */
    public JPanel getPanellogger() {
	return panellogger;
    }

    /**
     * @param panellogger
     *            the panellogger to set
     */
    public void setPanellogger(JPanel panellogger) {
	this.panellogger = panellogger;
    }

    /**
     * @return the panelCharger
     */
    public JPanel getPanelCharger() {
	return panelCharger;
    }

    /**
     * @param panelCharger
     *            the panelCharger to set
     */
    public void setPanelCharger(JPanel panelCharger) {
	this.panelCharger = panelCharger;
    }

    /**
     * @return the progresspanel
     */
    public JPanel getProgresspanel() {
	return progresspanel;
    }

    /**
     * @param progresspanel
     *            the progresspanel to set
     */
    public void setProgresspanel(JPanel progresspanel) {
	this.progresspanel = progresspanel;
    }

    /**
     * @return the splitPane
     */
    public JSplitPane getSplitPane() {
	return splitPane;
    }

    /**
     * @param splitPane
     *            the splitPane to set
     */
    public void setSplitPane(JSplitPane splitPane) {
	this.splitPane = splitPane;
    }

    /**
     * @return the dataPanel
     */
    public DataScrollPane getDataPanel() {
	return dataPanel;
    }

    /**
     * @param dataPanel
     *            the dataPanel to set
     */
    public void setDataPanel(DataScrollPane dataPanel) {
	this.dataPanel = dataPanel;
    }

    /**
     * @return the listdata
     */
    public JComboBox<String> getListdata() {
	return listdata;
    }

    /**
     * @param listdata
     *            the listdata to set
     */
    public void setListdata(JComboBox<String> listdata) {
	this.listdata = listdata;
    }

    /**
     * @return the startDate
     */
    public JDateChooser getStartDate() {
	return startDate;
    }

    /**
     * @param startDate
     *            the startDate to set
     */
    public void setStartDate(JDateChooser startDate) {
	this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public JDateChooser getEndDate() {
	return endDate;
    }

    /**
     * @param endDate
     *            the endDate to set
     */
    public void setEndDate(JDateChooser endDate) {
	this.endDate = endDate;
    }

    /**
     * @return the textarealogger
     */
    public JTextArea getTextarealogger() {
	return textarealogger;
    }

    /**
     * @param textarealogger
     *            the textarealogger to set
     */
    public void setTextarealogger(JTextArea textarealogger) {
	this.textarealogger = textarealogger;
    }

    /**
     * @return the charger
     */
    public JButton getCharger() {
	return charger;
    }

    /**
     * @param charger
     *            the charger to set
     */
    public void setCharger(JButton charger) {
	this.charger = charger;
    }


    /**
     * @return the screenSize
     */
    public static Dimension getScreenSize() {
	return screenSize;
    }

    /**
     * @param screenSize
     *            the screenSize to set
     */
    public static void setScreenSize(Dimension screenSize) {
	SmartFrame.screenSize = screenSize;
    }

    /**
     * @param width
     *            the width to set
     */
    public static void setWidth(int width) {
	SmartFrame.width = width;
    }

    /**
     * @param height
     *            the height to set
     */
    public static void setHeight(int height) {
	SmartFrame.height = height;
    }

    /**
     * @return the startCalendar
     */
    public Calendar getStartCalendar() {
	return startCalendar;
    }

    /**
     * @param startCalendar
     *            the startCalendar to set
     */
    public void setStartCalendar(Calendar startCalendar) {
	this.startCalendar = startCalendar;
    }

    /**
     * @return the endCalendar
     */
    public Calendar getEndCalendar() {
	return endCalendar;
    }

    /**
     * @param endCalendar
     *            the endCalendar to set
     */
    public void setEndCalendar(Calendar endCalendar) {
	this.endCalendar = endCalendar;
    }

    /**
     * @return the instrument
     */
    public String getInstrument() {
	return instrument;
    }

    /**
     * @param instrument
     *            the instrument to set
     */
    public void setInstrument(String instrument) {
	this.instrument = instrument;
    }

}
