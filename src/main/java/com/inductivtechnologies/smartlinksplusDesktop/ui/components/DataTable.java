package com.inductivtechnologies.smartlinksplusDesktop.ui.components;

import java.util.Vector;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
//import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;


/**
 * DataTable : classe qui étend JTable 
 * en vue d'être personnalisé
 * @author inductiv
 */
public class DataTable extends JTable implements ListSelectionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataTable() {
		// TODO Auto-generated constructor stub
	}

	public DataTable(TableModel dm) {
		super(dm);
		// TODO Auto-generated constructor stub
	}

	public DataTable(TableModel dm, TableColumnModel cm) {
		super(dm, cm);
		// TODO Auto-generated constructor stub
	}

	public DataTable(int numRows, int numColumns) {
		super(numRows, numColumns);
		// TODO Auto-generated constructor stub
	}

	public DataTable(Vector<?> rowData, Vector<?> columnNames) {
		super(rowData, columnNames);
		// TODO Auto-generated constructor stub
	}

	public DataTable(Object[][] rowData, Object[] columnNames) {
		super(rowData, columnNames);
		// TODO Auto-generated constructor stub
	}

	public DataTable(TableModel dm, TableColumnModel cm, ListSelectionModel sm) {
		super(dm, cm, sm);
		// TODO Auto-generated constructor stub
	}

	public void valueChanged(ListSelectionEvent e) {

//		if (e.getValueIsAdjusting())
//			return;
//
//		ListSelectionModel lsm = (ListSelectionModel) e.getSource();
//
//		if (lsm.isSelectionEmpty()) {
//
//		} else {
//			int minIndex = lsm.getMinSelectionIndex();
//			int maxIndex = lsm.getMaxSelectionIndex();
//			for (int i = minIndex; i <= maxIndex; i++) {
//				if (lsm.isSelectedIndex(i)) {
//
//				}
//			}
//
//		}
	}
}
