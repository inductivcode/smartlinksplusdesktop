package com.inductivtechnologies.smartlinksplusDesktop.ui.components;

import javax.swing.JScrollPane;

/**
 * DataScrollPane : Panel avec Scroll 
 * contenant DataTable
 * @author inductiv
 */

public class DataScrollPane extends JScrollPane {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DataScrollPane(DataTable compTable,int vsbPolicy, int hsbPolicy) {
		super(compTable,vsbPolicy, hsbPolicy);
//		compTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF );
	}

}
