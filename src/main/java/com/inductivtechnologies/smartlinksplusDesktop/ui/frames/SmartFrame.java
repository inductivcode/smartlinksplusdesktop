package com.inductivtechnologies.smartlinksplusDesktop.ui.frames;

import com.inductivtechnologies.smartlinksplusDesktop.callbacks.TokenCallback;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.AuthCallback;
import com.inductivtechnologies.smartlinksplusDesktop.callbacks.HttpCallback;
import com.inductivtechnologies.smartlinksplusDesktop.controllers.account.TokenController;
import com.inductivtechnologies.smartlinksplusDesktop.controllers.account.UserController;
import com.inductivtechnologies.smartlinksplusDesktop.controllers.market.CompanyController;
import com.inductivtechnologies.smartlinksplusDesktop.controllers.market.FxSpotSymbolController;
import com.inductivtechnologies.smartlinksplusDesktop.controllers.market.RawMarketDataController;
import com.inductivtechnologies.smartlinksplusDesktop.credential.CredentialReader;
import com.inductivtechnologies.smartlinksplusDesktop.credential.CredentialWriter;
import com.inductivtechnologies.smartlinksplusDesktop.models.account.Credential;
import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.Company;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusDesktop.ui.models.DataTableModel;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Constants;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Util;
import com.inductivtechnologies.smartlinksplusDesktop.writers.DataWriter;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import org.joda.time.DateTime;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * SmartFrame : Fenêtre principale 
*/
public class SmartFrame extends JFrame implements ActionListener{

    /**
	 * Pour la sérialisation 
	*/
	private static final long serialVersionUID = -9045057544618743100L;
	
    /**
	 * Utilisateur actuel 
	*/
    private String currentUser = System.getProperty("user.name");
    
    /**
	 * Pour afficher et récupérer les dates du JDateChooser 
	*/
    private static SimpleDateFormat standardDate = new SimpleDateFormat(Constants.PRINT_DATE_FORMAT);

    /**
	 * Le titre de la fenêtre  
	*/
    private String frameTitle;
    
    /**
	 * La table des données 
	*/
    private JTable dataTable;
    
    /**
     * Pour le model de la table des données  
	*/
    private DataTableModel dataTableModel;
    
    /**
     * Pour les notifications  
	*/
    public JLabel requestMessage;
    
    /**
     * Le panel principal
	*/
    private JPanel mainPanel;

    /**
     * Les différents boutons de la fénêtre 
	*/
    private JButton btnCompanies;
    private JButton btnFxSpotSymbol;
    private JButton btnEquities;
    private JButton btnFxSpot;
    private JButton btnExportData;

    /**
     * Pour le champ de texte symbole
	*/
    private JLabel labelSymbol;
    private JTextField tfSymbol;

    /**
     * Pour les champs de texte des dates 
	*/
    private JLabel labelStartDate;
    private JDateChooser startDate;
    private JLabel labelEndDate;
    private JDateChooser endDate;
    
    /**
     * Pour sauvegarder les différentes actions 
	*/
    private static String COMMAND_ACTION;
    
    //Callback(s)
    
    private HttpCallback httpCallback = new HttpCallback(){

    	@Override
    	public void allCompanies(List<Company> companies){
    		//Notification 
    		String message = "Chargement terminé : " + companies.size() + " Trouvés" ;
    		requestMessage.setText(message);
    		
    		//On affiche les donnéess
    		dataTableModel = new DataTableModel(EntitiesUtil
    					.convertListCompanyTo2DObject(companies), Constants.COMPANY_CSV_COLUMNS);
    		dataTableModel.setDataType(Constants.COMPANY_TYPE);
    		
    		dataTable.setModel(dataTableModel);
    		//On affiche les choses cachées 
    		enabledAllBtn(true);
    		//On affiche le bouton export data 
    		btnExportData.setEnabled(true);
    	}
    	
    	@Override
    	public void allFxSpotSymbols(List<FxSpotSymbol> symbols){
    		//Notification 
    		String message = "Chargement terminé : " + symbols.size() + " Trouvés" ;
    		requestMessage.setText(message);
    		
    		//On affiche les donnéess
    		dataTableModel = new DataTableModel(EntitiesUtil
    					.convertListFxSpotSymbolTo2DObject(symbols), Constants.FXSPOT_SYMBOL_CSV_COLUMNS);
    		dataTableModel.setDataType(Constants.FXSPOT_SYMBOLS_TYPE);
    		
    		dataTable.setModel(dataTableModel);
    		//On affiche les choses cachées 
    		enabledAllBtn(true);
    		//On affiche le bouton export data 
    		btnExportData.setEnabled(true);
    	}
    	
    	@Override
    	public void equitiesBetweenTwoDate(List<RawMarketData> equities, String symbol){
    		//Notification 
    		String message = "Chargement terminé : " + equities.size() + " Trouvés" ;
    		requestMessage.setText(message);
    		
    		//On affiche les donnéess
    		dataTableModel = new DataTableModel(EntitiesUtil
    					.convertListEquitiesTo2DObject(equities), Constants.EQUITY_CSV_COLUMNS);
    		dataTableModel.setSymbol(symbol);
    		dataTableModel.setDataType(Constants.EQUITY_TYPE);
    		
    		dataTable.setModel(dataTableModel);
    		//On affiche les choses cachées 
    		enabledAllBtn(true);
    		btnExportData.setEnabled(true);
    	}
    	
    	@Override
    	public void fxSpotBetweenTwoDate(List<RawMarketData> fxspots, String symbol){
    		//Notification 
    		String message = "Chargement terminé : " + fxspots.size() + " Trouvés" ;
    		requestMessage.setText(message);

    		//On affiche les donnéess
    		dataTableModel = new DataTableModel(EntitiesUtil
    					.convertListFxSpotTo2DObject(fxspots), Constants.FXSPOT_CSV_COLUMNS);
    		dataTableModel.setSymbol(symbol);
    		dataTableModel.setDataType(Constants.FXSPOT_TYPE);
    		
    		dataTable.setModel(dataTableModel);
    		//On affiche les choses cachées 
    		enabledAllBtn(true);
    		btnExportData.setEnabled(true);
    	}
    	
    	@Override
		public void displayNotification(String notificationMessage){
			requestMessage.setText(notificationMessage);
		}

    	@Override
    	public void onFailure(String notificationMessage) {
    		//Notification 
    		requestMessage.setText("Chargement terminé : " + notificationMessage);
    		//On vide le tableau de données 
    		dataTableModel = null;
    		dataTable.setModel(new DefaultTableModel());
    		//On affiche les choses cachées 
    		enabledAllBtn(true);
    		//On cache le bouton export data 
    		btnExportData.setEnabled(false);
    	}
    	
    };
    
    private TokenCallback tokenCallback = new TokenCallback(){

		@Override
		public void checkValidityToken(boolean result, String tokenValue){
			if(result && tokenValue != null){
				//Token valide 
				if(COMMAND_ACTION.equals(Constants.BTN_COMPANIES_CM)){
			        //On cache tout 
			        enabledAllBtn(false);
			        btnExportData.setEnabled(false);
			        //On charge les compagnies 
			        new CompanyController(tokenValue).allCompanies(httpCallback);
			    }
				
				if(COMMAND_ACTION.equals(Constants.BTN_FXSPOT_SYMBOLS_CM)){
					//On cache tout 
		            enabledAllBtn(false);
		        	btnExportData.setEnabled(false);
		            //On charge les symboles 
		            new FxSpotSymbolController(tokenValue).allFxSpotSymbols(httpCallback);
			    }
				
				if(COMMAND_ACTION.equals(Constants.BTN_EQUITIES_CM)){
			        equitiesAndFxSpotAction(Constants.EQUITY_TYPE, tokenValue);
			    }

			    if(COMMAND_ACTION.equals(Constants.BTN_FXSPOT_CM)){
			        equitiesAndFxSpotAction(Constants.FXSPOT_TYPE, tokenValue);
			    }
			}
		}

		@Override
		public void displayNotification(String notificationMessage){
			requestMessage.setText(notificationMessage);
		}

		@Override
		public void onFailure(String errorMessage){
			//Notification 
			requestMessage.setText(errorMessage);
			//On vide le tableau de données 
			dataTableModel = null;
			dataTable.setModel(new DefaultTableModel());
			//On affiche les choses cachées 
			enabledAllBtn(true);
			//On cache le bouton export data 
			btnExportData.setEnabled(false);
		}
    	
    };
    
    private AuthCallback authCallback = new AuthCallback(){
    	
		@Override
		public void authUser(Token token){
			if(token != null) new TokenController().checkValidityToken(tokenCallback, token);
		}

    	@Override
		public void displayNotification(String notificationMessage){
			requestMessage.setText(notificationMessage);
		}

		@Override
		public void onFailure(String errorMessage){
			//Notification 
			requestMessage.setText(errorMessage);
			//On affiche les choses cachées 
			enabledAllBtn(true);
			//On cache le bouton export data 
			btnExportData.setEnabled(false);
		}

    };
    
    
    /**
     * Constructeur 
	*/
    public SmartFrame(String frameTitle){
        this.frameTitle = frameTitle;

        setTitle(frameTitle);
        setSize(700,600);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        
        //Icone 
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/smartlinksplus_logo.png")));
        
        
        setResizable(true);
        setLocationRelativeTo(null);
        setUndecorated(false);

        setContentPane(contentPane());
        setBackground(Color.WHITE);

        //On ajoute les écouteurs 
        addListenners();

        //On affiche la fénêtre
        setVisible(true);
    }

    /**
     * Initialisation du conteneur de la frame 
	*/
    private Container contentPane(){
        mainPanel = new JPanel(new BorderLayout());
        mainPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        //Pour le JTable
        initDataTable();
        ////////////////////////////////////////

        //Pour les boutons d'actions
        initActionPanel();
        ///////////////////////////////////////////

        //Le panel des notifications
        notificationPanel();
        //////////////////////////////////////

        return mainPanel;
    }

    /**
     * Initialisation du conteneur des boutons d'actions
	*/
    private void initActionPanel(){
        JPanel actionPanel = new JPanel(new BorderLayout());
        actionPanel.setBorder(BorderFactory.createEmptyBorder(8, 12, 8, 12));

        JPanel northPanel = new JPanel();
        northPanel.setLayout(new BoxLayout(northPanel, BoxLayout.Y_AXIS));
        JPanel southPanel = new JPanel();
        southPanel.setLayout(new BoxLayout(southPanel, BoxLayout.Y_AXIS));

        //On construit les boutons
        buildBtnsActions();

        //Le choix du symbole
        buildInputSymbol();

        //Les sélecteurs de date
        buildDateChooser();

        //On ajoute les panels principaux
        mainPanel.add(actionPanel, BorderLayout.EAST);
        actionPanel.add(northPanel, BorderLayout.NORTH);
        actionPanel.add(southPanel, BorderLayout.SOUTH);

        //On ajoute le input pour entrer un symbole
        northPanel.add(labelSymbol);
        northPanel.add(Box.createRigidArea(new Dimension(0, 4)));
        northPanel.add(tfSymbol);

        //On ajoute les sélecteurs de date
        northPanel.add(Box.createRigidArea(new Dimension(0, 4)));
        northPanel.add(labelStartDate);
        northPanel.add(Box.createRigidArea(new Dimension(0, 4)));
        northPanel.add(startDate);
        northPanel.add(Box.createRigidArea(new Dimension(0, 4)));
        northPanel.add(labelEndDate);
        northPanel.add(Box.createRigidArea(new Dimension(0, 4)));
        northPanel.add(endDate);

        //On ajoute les boutons des actions
        northPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        northPanel.add(btnEquities);
        northPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        northPanel.add(btnFxSpot);
        northPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        northPanel.add(btnCompanies);
        northPanel.add(Box.createRigidArea(new Dimension(0, 10)));
        northPanel.add(btnFxSpotSymbol);

        //On ajoute les boutons du bas
        southPanel.add(btnExportData);
    }

    /**
     * Initialisation de la table des données 
	*/
    @SuppressWarnings("serial")
	private void initDataTable(){
        JPanel dataPanel = new JPanel(new BorderLayout());
        dataPanel.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 0));

        dataTable = new JTable();
        dataTable.setShowGrid(false);
        dataTable.setIntercellSpacing(new Dimension(0, 0));
        dataTable.setRowHeight(30);
        dataTable.setFont(new Font("Default", Font.PLAIN, 12));
        dataTable.setSelectionBackground(Color.decode("#E3F2FD"));
        dataTable.setDefaultRenderer(Object.class, new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                           boolean hasFocus, int row, int column) {
                Border padding = BorderFactory.createEmptyBorder(0, 4, 0, 0);
                final Component component = super
                        .getTableCellRendererComponent(table, value, false, false, row, column);
                component.setBackground(row % 2 == 0 ? Color.WHITE : Color.decode("#F5F5F5"));
                setBorder(BorderFactory.createCompoundBorder(getBorder(), padding));
                return component;
            }
        });

        JScrollPane dataScroll = new JScrollPane(dataTable);
        dataScroll.getViewport().setBackground(Color.WHITE);
        mainPanel.add(dataPanel, BorderLayout.CENTER);
        dataPanel.add(dataScroll, BorderLayout.CENTER);
    }

    /**
     * Initialisation du conteneur des notifications 
	*/
    private void notificationPanel(){
        JPanel notificationPanel = new JPanel(new BorderLayout());
        notificationPanel.setBackground(Color.decode("#212121"));
        Border padding = BorderFactory.createEmptyBorder(4, 8, 4, 10);
        notificationPanel.setBorder(BorderFactory.createCompoundBorder(notificationPanel.getBorder(), padding));
        Font notifMessageStyle = new Font("Arial", Font.BOLD, 10);

        requestMessage = new JLabel("");
        requestMessage.setForeground(Color.WHITE);
        requestMessage.setFont(notifMessageStyle);

        JLabel welcomeMessage = new JLabel("Bienvenue " + currentUser);
        welcomeMessage.setForeground(Color.WHITE);
        welcomeMessage.setFont(notifMessageStyle);

        mainPanel.add(notificationPanel, BorderLayout.SOUTH);
        notificationPanel.add(requestMessage, BorderLayout.EAST);
        notificationPanel.add(welcomeMessage, BorderLayout.WEST);
    }

    /**
     * Construction des boutons 
	*/
    private void buildBtnsActions(){
        btnCompanies = new JButton("Compagnies");
        btnCompanies.setFocusPainted(false);
        btnCompanies.setBorderPainted(false);
        btnCompanies.setAlignmentX(Component.LEFT_ALIGNMENT);
        btnCompanies.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));

        btnFxSpotSymbol = new JButton("Symboles FXSPOT");
        btnFxSpotSymbol.setFocusPainted(false);
        btnFxSpotSymbol.setBorderPainted(false);
        btnFxSpotSymbol.setAlignmentX(Component.LEFT_ALIGNMENT);
        btnFxSpotSymbol.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));

        btnEquities = new JButton("Equities");
        btnEquities.setFocusPainted(false);
        btnEquities.setBorderPainted(false);
        btnEquities.setAlignmentX(Component.LEFT_ALIGNMENT);
        btnEquities.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));

        btnFxSpot = new JButton("FxSpot");
        btnFxSpot.setFocusPainted(false);
        btnFxSpot.setBorderPainted(false);
        btnFxSpot.setAlignmentX(Component.LEFT_ALIGNMENT);
        btnFxSpot.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));

        btnExportData = new JButton("Exporter vers");
        btnExportData.setEnabled(false);
        btnExportData.setFocusPainted(false);
        btnExportData.setBorderPainted(false);
        btnExportData.setAlignmentX(Component.LEFT_ALIGNMENT);
        btnExportData.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
    }

    /**
     * Initialisation des composants afin de choisir des dates 
	*/
    private void buildDateChooser(){
        labelStartDate = new JLabel("Date de début");
        labelStartDate.setAlignmentX(Component.LEFT_ALIGNMENT);
        labelStartDate.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
        startDate = new JDateChooser(new Date(new DateTime().minusMonths(1).getMillis()));
        startDate.setAlignmentX(Component.LEFT_ALIGNMENT);
        startDate.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));

        labelEndDate = new JLabel("Date de fin");
        labelEndDate.setAlignmentX(Component.LEFT_ALIGNMENT);
        labelEndDate.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
        endDate = new JDateChooser(new Date());
        endDate.setAlignmentX(Component.LEFT_ALIGNMENT);
        endDate.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
    }

    /**
     * Initialisation du composant dans lequel renseigné le symbole
	*/
    private void buildInputSymbol(){
        labelSymbol = new JLabel("Symbole");
        labelSymbol.setAlignmentX(Component.LEFT_ALIGNMENT);
        labelSymbol.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
        tfSymbol = new JTextField("");
        tfSymbol.setAlignmentX(Component.LEFT_ALIGNMENT);
        tfSymbol.setMaximumSize(new Dimension(Short.MAX_VALUE,  40));
    }

    /**
     * Initialisation des écouteurs 
	*/
    private void addListenners(){
        btnCompanies.setActionCommand(Constants.BTN_COMPANIES_CM );
        btnCompanies.addActionListener(this);

        btnFxSpotSymbol.setActionCommand(Constants.BTN_FXSPOT_SYMBOLS_CM );
        btnFxSpotSymbol.addActionListener(this);

        btnEquities.setActionCommand(Constants.BTN_EQUITIES_CM );
        btnEquities.addActionListener(this);

        btnFxSpot.setActionCommand(Constants.BTN_FXSPOT_CM );
        btnFxSpot.addActionListener(this);

        btnExportData.setActionCommand(Constants.BTN_EXPORT_DATA_CM );
        btnExportData.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				 btnExportDataAction();
			}
        });
    }

    /**
     * Récupère la date contenu dans le sélecteur de date 
     * @param dateChooser Le composant date 
	*/
    private String getDateInJDateChooser(JDateChooser dateChooser){
        return standardDate.format(dateChooser.getDate());
    }

    /**
     * Cache ou affiche tous les boutons 
     * @param state Vrai pour visible et Faux pour invisible 
	*/
    public void enabledAllBtn(boolean state){
        btnCompanies.setEnabled(state);
        btnFxSpotSymbol.setEnabled(state);
        btnFxSpot.setEnabled(state);
        btnEquities.setEnabled(state);
    }
    
    /**
     * Définie l'action à éffectuée lorsqu'on
     * clique sur le bouton equities ou fxspot
     * @param actionType Type d'action EQUITIES ou FXSPOT 
	*/
    private void equitiesAndFxSpotAction(String actionType, String tokenValue){    
    	String symbol = tfSymbol.getText();
        String strStartDate = getDateInJDateChooser(startDate);
        String strEndDate = getDateInJDateChooser(endDate);
        
       //On cache tout 
    	enabledAllBtn(false);
      	btnExportData.setEnabled(false);
        //On charge les equities 
        switch(actionType){
			case Constants.EQUITY_TYPE :
		         new RawMarketDataController(tokenValue)
	            	.rawMarketDataBetweenTwoDate(httpCallback, 
	            			Constants.EQUITY_TYPE,
	            			symbol.toUpperCase(),
	            			strStartDate, 
	            			strEndDate);
				break;
			case Constants.FXSPOT_TYPE :
				new RawMarketDataController(tokenValue)
            		.rawMarketDataBetweenTwoDate(httpCallback,
            				Constants.FXSPOT_TYPE,
            				symbol.toUpperCase(),
            				strStartDate, 
            				strEndDate);
				break;
	    }

    }
    
    /**
     * Définie l'action pour exporter les données dans un fichier externe 
	*/
    private void btnExportDataAction(){
    	String exportFileName = null;
    	
        if(dataTableModel != null){
            int rowCount = dataTableModel.getRowCount();
            int columnCount = dataTableModel.getColumnCount();

            if(rowCount != 0 && columnCount != 0){
                JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
                fileChooser.setDialogTitle("Exporter les données");

                //Restriction sur le filtre
                fileChooser.setAcceptAllFileFilterUsed(false);
                //Uniquement les fichier csv
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier CSV", "csv");
                fileChooser.addChoosableFileFilter(filter);
                
                //Nom par défaut du fichier à exporter
                if(dataTableModel.getSymbol() != null){
                    exportFileName = String.format(Constants.CSV_DATA_FILE_NAME, dataTableModel.getSymbol());
                   
                }else{
                	switch(dataTableModel.getDataType()) {
	                	case Constants.COMPANY_TYPE : 
	                		 exportFileName = Constants.CSV_COMPANY_FILE_NAME;
	                		 break;
	                	case Constants.FXSPOT_SYMBOLS_TYPE : 
	                		 exportFileName = Constants.CSV_FXSPOT_SYMBOL_FILE_NAME;
	                		 break;
                	}
                	
                }

                fileChooser.setSelectedFile(new File(exportFileName));
                
                //On ouvre le file chooser
                int userSelection  = fileChooser.showSaveDialog(this);

                if (userSelection  == JFileChooser.APPROVE_OPTION) {
                    //On récupère le fichier sélectionné
                    File selectedFile = fileChooser.getSelectedFile();

                    //Est ce un fichier csv ?
                    if (!selectedFile.getName().toLowerCase().endsWith(".csv")) {
                        selectedFile = new File(selectedFile.getParentFile(),
                                Util.filenameWithoutExtension(selectedFile.getName()) + ".csv");
                    }

                    //On créé le fichier si il n'existe pas
                    if(Util.createFileIfNotExist(selectedFile.getAbsolutePath())){
                        //On écrit dans le fichier
                        requestMessage.setText("Exportation en cours ...");
                        
                        File result = DataWriter.exportDataFromJTable(selectedFile.getAbsolutePath(), 
                        		dataTableModel.getData(), dataTableModel.getColumns());
                        
                        //On notifie
                        requestMessage.setText("Données exportées vers : " + result.getAbsolutePath());
                    }
                }
            }
        }
    }
    
    /**
     * Permet de récupérer le token depuis le 
     * fichier et de lancer la vérification 
	*/
    private void tokenAction(){
    	TokenController tokenController = new TokenController();
    	  //On lit le token 
        Token token = CredentialReader.readCredentialInfos();
    	
    	if(token != null){
         	tokenController.checkValidityToken(tokenCallback, token);
    	}else{
    		//Token absent alors on lance une authentification 
    		  new UserController()
    	    	.authUser(authCallback, new Credential(CredentialWriter.getUsername(),
    	    			CredentialWriter.getPassword()));
    	}
    }
    
    //Override

    /**
     * Gestion des évènements des boutons 
	*/
    @Override
    public void actionPerformed(ActionEvent event){;
        COMMAND_ACTION = event.getActionCommand();
        
        if(COMMAND_ACTION.equals(Constants.BTN_EQUITIES_CM) || 
        		COMMAND_ACTION.equals(Constants.BTN_EQUITIES_CM)){
        	
            if(!getDateInJDateChooser(startDate).isEmpty() && 
            		!getDateInJDateChooser(endDate).isEmpty() && 
            		!tfSymbol.getText().isEmpty()){
            	 //On vérifie le token 
            	tokenAction();
            }
        }else{
        	//On vérifie le token 
        	tokenAction();
        }
      
    }
	
    ///Getters et setters

    public String getFrameTitle() {
        return frameTitle;
    }

    public void setFrameTitle(String frameTitle) {
        this.frameTitle = frameTitle;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(String currentUser) {
        this.currentUser = currentUser;
    }

    public JButton getBtnCompanies() {
        return btnCompanies;
    }

    public void setBtnCompanies(JButton btnCompanies) {
        this.btnCompanies = btnCompanies;
    }

    public JButton getBtnFxSpotSymbol() {
        return btnFxSpotSymbol;
    }

    public void setBtnFxSpotSymbol(JButton btnFxSpotSymbol) {
        this.btnFxSpotSymbol = btnFxSpotSymbol;
    }

    public JButton getBtnEquities() {
        return btnEquities;
    }

    public void setBtnEquities(JButton btnEquities) {
        this.btnEquities = btnEquities;
    }

    public JButton getBtnFxSpot() {
        return btnFxSpot;
    }

    public void setBtnFxSpot(JButton btnFxSpot) {
        this.btnFxSpot = btnFxSpot;
    }

    public JButton getBtnExportData() {
        return btnExportData;
    }

    public void setBtnExportData(JButton btnExportData) {
        this.btnExportData = btnExportData;
    }

    public JTextField getTfSymbol() {
        return tfSymbol;
    }

    public void setTfSymbol(JTextField tfSymbol) {
        this.tfSymbol = tfSymbol;
    }

    public JDateChooser getStartDate() {
        return startDate;
    }

    public void setStartDate(JDateChooser startDate) {
        this.startDate = startDate;
    }

    public JDateChooser getEndDate() {
        return endDate;
    }

    public void setEndDate(JDateChooser endDate) {
        this.endDate = endDate;
    }

    public JTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(JTable dataTable) {
        this.dataTable = dataTable;
    }

	public JLabel getRequestMessage() {
		return requestMessage;
	}

	public void setRequestMessage(JLabel requestMessage) {
		this.requestMessage = requestMessage;
	}

	public static String getCOMMAND_ACTION() {
		return COMMAND_ACTION;
	}

	public static void setCOMMAND_ACTION(String cOMMAND_ACTION) {
		COMMAND_ACTION = cOMMAND_ACTION;
	}
}
