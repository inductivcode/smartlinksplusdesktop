package com.inductivtechnologies.smartlinksplusDesktop.ui.models;

import javax.swing.table.AbstractTableModel;

/**
 * DataTableModel :Modèle utilisé pour afficher les données dans le JTable 
*/
public class DataTableModel extends AbstractTableModel {

	/**
	 * Pour la sérialisation
	*/
	private static final long serialVersionUID = -889343131140375843L;

	/**
	 * data : Les donnees
	*/
	private Object data[][];
	
	/**
	 * columns : Les colonnes
	*/
	private String columns[];
	
	/**
	 * symbol : Le symbole correspondant aux données data 
	 * ou la chaine vide si les données ne sont pas associées à un symbole 
	 * Ex Les données sur les compagnies 
	*/
	private String symbol;
	
	/**
	 * dataType : Le type de données EQUITIES , COMPANY ...
	*/
	private String dataType;

	/**
	 * Constructeur 
	*/
	public DataTableModel() {
	}

	/**
	 * Constructeur 
	*/
	public DataTableModel(Object[][] data, String[] columns) {
		this.data = data;
		this.columns = columns;
	}

	//Getters et setters 
	
	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columns[col];
	}

	public int getColumnCount() {
		return data[0].length;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		return data[rowIndex][columnIndex];
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public Object[][] getData() {
		return data;
	}

	public void setData(Object[][] data) {
		this.data = data;
	}

	public String[] getColumns() {
		return columns;
	}

	public void setColumns(String[] columns) {
		this.columns = columns;
	}
}
