package com.inductivtechnologies.smartlinksplusDesktop.ui.tools;

/**
 * WhatIDo : Impléménte un simple LOG dans un JTextArea
 */
public class WhatIDo {
	 
	 /**
	  * Affiche un simple message avec un tag
	  * @param tag Le tag 
	  * @param message : Le message à afficher 
	  */
	 public static void message(String tag, String message) {
		 System.out.println(tag + " ::: " + message); 
	 }
}
