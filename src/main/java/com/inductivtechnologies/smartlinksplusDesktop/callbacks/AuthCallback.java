package com.inductivtechnologies.smartlinksplusDesktop.callbacks;

import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;

/**
 * AuthCallback : Callback pour l'authentification 
 */
public interface AuthCallback extends BaseCallback{

	/**
	 * Récupère le token après authentification
	 * @param token Le token récupéré
	*/
	public void authUser(Token token);
	
}
