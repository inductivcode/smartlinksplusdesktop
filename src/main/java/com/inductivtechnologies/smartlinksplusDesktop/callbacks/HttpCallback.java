package com.inductivtechnologies.smartlinksplusDesktop.callbacks;

import java.util.List;

import com.inductivtechnologies.smartlinksplusDesktop.models.market.Company;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;

/**
 * Callback pour récupérer les données HTTP depuis les interfaces utilisateur
 */
public interface HttpCallback extends BaseCallback{
	
	/**
	 * Récupère toutes les compagnies
	 * @param companies La liste des compagnies 
	*/
	public void allCompanies(List<Company> companies);
	
	/**
	 * Récupère tous les symboles des FXSPOT
	 * @param companies La liste des symboles 
	*/
	public void allFxSpotSymbols(List<FxSpotSymbol> symbols);
	
	/**
	 * Récupère des equities entre deux dates 
	 * @param symbol Le symbole 
	*/
	public void equitiesBetweenTwoDate(List<RawMarketData> markets, String symbol);
	
	/**
	 * Récupère des fx spot entre deux dates 
	 * @param symbol Le symbole 
	*/
	public void fxSpotBetweenTwoDate(List<RawMarketData> fxspots, String symbol);
}
