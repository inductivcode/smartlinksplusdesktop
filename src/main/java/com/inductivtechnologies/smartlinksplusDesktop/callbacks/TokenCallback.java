package com.inductivtechnologies.smartlinksplusDesktop.callbacks;

/**
 * TokenCallback : Callback pour le token 
 */
public interface TokenCallback extends BaseCallback{
	
	/**
	 * Callback déclenché pour valider un token 
	 * @param result le résultat de la validation 
	 * @param tokenValue Le token validé
	*/
	public void checkValidityToken(boolean result, String tokenValue);

}
