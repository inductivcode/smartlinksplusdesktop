package com.inductivtechnologies.smartlinksplusDesktop.callbacks;

/**
 * BaseCallback : Callback de base
 */
public interface BaseCallback{

	/**
	 * Callback déclenché pour afficher un message de notification 
	 * @param message Le message de notification 
	*/
	public void displayNotification(String notificationMessage);
	
	/**
	 * Callback déclenché en cas d'erreur 
	 * @param errorMessage Le message de notification 
	*/
	public void onFailure(String errorMessage);
	
}
