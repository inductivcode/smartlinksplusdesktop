package com.inductivtechnologies.smartlinksplusDesktop.services.account;

import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * TokenService : Permer d'accéder aux différents services
 * proposés par l'api sur la ressource Token
 */
public interface TokenService {

	/**
	 * Vérifit l'existence et la validité d'un token 
	 * @param value La valeur du token  
	*/
	 @GET("token/check")
	 Call<ApiResponse> checkToken(@Query("value") final String value);
	
}
