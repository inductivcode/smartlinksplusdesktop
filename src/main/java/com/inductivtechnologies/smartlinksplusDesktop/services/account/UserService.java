package com.inductivtechnologies.smartlinksplusDesktop.services.account;

import com.inductivtechnologies.smartlinksplusDesktop.models.account.Credential;
import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * UserService : Permer d'accéder aux différents services
 * proposés par l'api sur la ressource User
 */
public interface UserService {

	/**
	 * Authentifie un utilisateur à partir de son nom utilisateur et son mot de passe 
	 * @param credential Les informations d'authentification 
	 * @return Le token de l'utilisateur correspondant ou null si aucun ne correspond
	*/
	 @Headers("Content-Type: application/json")
	 @POST("user/auth")
	 Call<ApiResponse> authenticateUser(@Body Credential credential);
}
