package com.inductivtechnologies.smartlinksplusDesktop.services.market;

import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * CompanyService : Permer d'accéder aux différents services
 * proposés par l'api sur la ressource Company
 */
public interface CompanyService {
	
	/**
	 * Retorune toutes les compagnies
	 * @return La liste de toutes compagnies 
	*/
	 @GET("data/company/all")
	 Call<ApiResponse> allCompanies();
	
}
