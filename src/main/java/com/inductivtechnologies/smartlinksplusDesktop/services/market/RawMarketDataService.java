package com.inductivtechnologies.smartlinksplusDesktop.services.market;

import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * RawMarketDataService : Permer d'accéder aux différents services
 * proposés par l'api sur la ressource RawMarketData
 */
public interface RawMarketDataService {
	
	/**
	 * Retourne les equities entre deux dates 
	 * @param symbol Le symbole 
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	 * @return La liste des equities entre startDate et endDate au format JSON 
	*/
	 @GET("data/market/equity/{symbol}")
	 Call<ApiResponse> equitiesBetweenTwoDate(@Path("symbol") final String symbol, 
			 @Query("startDate") final String startDate,
			 @Query("endDate") final String endDate);
	
	/**
	 * Retourne les fx spot entre deux dates 
	 * @param symbol Le symbole 
	 * @param startDate Date de début 
	 * @param endDate Date de fin 
	 * @return La liste des fx spot entre startDate et endDate
	*/
	 @GET("data/market/fxspot/{symbol}")
	 Call<ApiResponse> fxspotBetweenTwoDate(@Path("symbol") final String symbol, 
			 @Query("startDate") final String startDate,
			 @Query("endDate") final String endDate);
	
	 
}
