package com.inductivtechnologies.smartlinksplusDesktop.services.market;

import com.inductivtechnologies.smartlinksplusDesktop.models.util.ApiResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * FxSpotSymbolService : Permer d'accéder aux différents services
 * proposés par l'api sur la ressource FxSpotSymbol
 */
public interface FxSpotSymbolService {
	 
	/**
	 * Retourne tous les symbols fx spot
	 * @return La liste des symboles
	*/
	 @GET("data/fxspotsymbol/all")
	 Call<ApiResponse> allFxSpotSymbols();
}
