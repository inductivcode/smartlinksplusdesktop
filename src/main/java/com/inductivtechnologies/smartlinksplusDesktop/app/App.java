package com.inductivtechnologies.smartlinksplusDesktop.app;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.inductivtechnologies.smartlinksplusDesktop.ui.frames.SmartFrame;

/**
 * Classe main pour le lancement de l'application
 */
public class App {
	
	public static void main(String ...strings) {
		
		 String lookAndFeelName = UIManager.getSystemLookAndFeelClassName();
	     try {
	         UIManager.setLookAndFeel(lookAndFeelName);
	     }catch(UnsupportedLookAndFeelException ex) {
	         ex.printStackTrace();
	     }catch(IllegalAccessException ex) {
	         ex.printStackTrace();
	     }catch(InstantiationException ex) {
	         ex.printStackTrace();
	     }catch(ClassNotFoundException ex) {
	         ex.printStackTrace();
	     }

	     new SmartFrame("SmartLinksPlus");
	}
}
