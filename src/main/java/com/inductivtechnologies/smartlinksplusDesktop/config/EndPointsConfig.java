package com.inductivtechnologies.smartlinksplusDesktop.config;

/**
 * EndPointsConfig : Contient quelques configurations relatives à l'invocation 
 * de L'API SmartLinksPlus 
 */
public class EndPointsConfig {
	
	//Url de base 
//	public final static String API_BASE_URL = "http://www.inductiv-risk.com/smartlinksplus/api/";
//	public final static String API_BASE_URL = "http://192.168.100.20:8080/smartlinksplus/api/";
	public final static String API_BASE_URL = "http://127.0.0.1:8080/smartlinksplus/api/";
	
}
