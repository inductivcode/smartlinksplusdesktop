package com.inductivtechnologies.smartlinksplusDesktop.util.data;

import java.io.File;
import java.io.IOException;

import com.inductivtechnologies.smartlinksplusDesktop.models.account.Token;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;

/**
 * Util : Utilitaire 
 */
public class Util {
	
	public static final String TAG = Util.class.getSimpleName();
	
	public static final String DocumentsDir = joinPath(System.getProperty("user.home"), "Documents");
	public static final String appDir = joinPath(DocumentsDir, "SmartLinksPlus");
	
	public static final String dataDir = joinPath(appDir, "Data");
	public static final String equityDir = joinPath(dataDir, "Equities");
	public static final String fxSpotDir = joinPath(dataDir, "FxSpots");
	
	public static final String secretDir = joinPath(appDir, "Secret");
	public static final String credentialFile = joinPath(secretDir, "CredentialToken.csv");
	
	/**
	 * Construit un objet Token à partir d'une ligne du fichier CSV
	 * @param columns - Les colonnes correspondant à une ligne du fichier csv
	 * @return Token> : Le token construit  
	 */
	public static Token buildToken(String[] columns){
		Token token = new Token();
		
		if(columns != null && columns.length == 4) {
			token.setIdToken(columns[0]);
			token.setValue(columns[1]);
			token.setExpirationDate(DateUtils.parseJavaDate(columns[2], Constants.US_DATETIME_FORMAT));
			token.setCreationDate(DateUtils.parseJavaDate(columns[3], Constants.US_DATETIME_FORMAT));
		}
		
		return token;
	}
	
	/**
     * Donné l'accès à un fichier s'il existe sinon le crée 
     * @param absolutePathFile Le chemin absolu du fichier  
     * @return boolean Vrai si tout s'est bien passe Faux sinon
     */
	public static boolean createFileIfNotExist(String absolutePathFile){
		boolean createFileResult = true;
		
		//On récupère le fichier s'il existe sinon on le crée
		File file = new File(absolutePathFile);
		
		if(!file.exists()){
			try {
				createFileResult = file.createNewFile();
			}catch (IOException exception) {
				createFileResult = false;
				WhatIDo.message(TAG, "Fichier non créé");
				exception.printStackTrace();
			}
		}
		
		return createFileResult;
	}
	
	/**
     * Donné l'accès à un repertoire s'il existe sinon on le crée
     * @param absolutePath Le chemin absolu du repertoire  
     * @return boolean Vrai si tout s'est bien passe Faux sinon
     */
	public static boolean createDirectoryIfNotExist(String absolutePath){
		boolean createFileResult = true;
		
		//On récupère le repertoire s'il existe sinon on le crée
		File dir = new File(absolutePath);
		
		if(!dir.exists()) createFileResult = dir.mkdir();
		
		return createFileResult;
	}
	
	/**
     * Concatene deux chemins de fichiers
     * @param path1 Le premier chemin 
     * @param path2 Le second chemin 
     * @return Le chemin obtenu 
     */
	public static String joinPath(String path1, String path2) {
	    File file1 = new File(path1);
	    File file2 = new File(file1, path2);
	    
	    return file2.getAbsolutePath();
	}
	
	/**
     * Permet de générer l'aborescence des dossiers pour
     * l'application au lancement 
     * @return Vrai si l'aborescence a été créée Faux sinon 
     */
	public static boolean generateApplicationTree(){
		System.out.println("[Util] ::: Génération de l'aborescence de l'application");
		File file = new File(DocumentsDir);
		
		if(file.exists() && file.isDirectory()) {
			if(createDirectoryIfNotExist(appDir) && 
					createDirectoryIfNotExist(secretDir)) {
				System.out.println("[Util] ::: Génération de l'aborescence terminée");
				return true;
			}
		}
		System.out.println("[Util] ::: Génération de l'aborescence non aboutie");
		return false;
	}
	
	/**
	 * Construit une chaine de caractères à partir d'un Token afin de l'inserer dans un fichier .
	 * @param token La donnée à transformer
	 * @return String  
	 */
	public static String transformTokenToString(Token token){
		String lineValues  = token.getIdToken().toString() + "," +
				token.getValue() + "," +
				DateUtils.printJavaDate(token.getExpirationDate(), Constants.US_DATETIME_FORMAT) + "," + 
				DateUtils.printJavaDate(token.getCreationDate(), Constants.US_DATETIME_FORMAT);
		
		return lineValues;
	}
	
	/**
	 * Construit une chaine de caractères à partir d'un 
	 * marché afin de l'inserer dans un fichier csv Ex : 12-03-2018, 0.345, 0.345, 0.345 ...
	 * @param type Le type du marché EQUITIES OU FXSPOT
	 * @param market La donnée à transformer
	 * @return String  
	 */
	public static String transformRawMarketDataToString(String type, RawMarketData market){
		String lineValues  = "";
		
		switch(type){
			case Constants.EQUITY_TYPE : 
				lineValues = DateUtils.printJavaDate(market.getRetrievedDate(), Constants.PRINT_DATE_FORMAT) + "," + 
						String.valueOf(market.getOpenedValue()) + "," + 
						String.valueOf(market.getHighestValue()) + "," +
						String.valueOf(market.getLowestValue()) + "," +
						String.valueOf(market.getClosedValue()) + "," +
						String.valueOf(market.getAdjclose()) + "," +
						String.valueOf(market.getVolume());
				break;
			case Constants.FXSPOT_TYPE : 
				lineValues = DateUtils.printJavaDate(market.getRetrievedDate(), Constants.PRINT_DATE_FORMAT) + "," + 
						String.valueOf(market.getClosedValue()) + "," + 
						String.valueOf(market.getOpenedValue()) + "," +
						String.valueOf(market.getHighestValue()) + "," +
						String.valueOf(market.getLowestValue()) + "," +
						market.getChangeValue();
				break;
		}
		
		return lineValues;
	}
	
	
	/**
     * Retourne la position dune chaine dans un tableau
     * @param source Chaine source 
     * @param search Elément à chercher
     * @return Position de la chaine
     */
    public static int positionOfStringInArray(String[] source, String search){
        for(int i = 0; i < source.length; i++){
            if(source[i].equalsIgnoreCase(search)) return i;
        }
        return -1;
    }
    
	/**
     * Transforme un tableau en une ligne de fichier CSV 
     * Ex nom,prénom, etc
     * @param data Le tableau à transformer
     * @return Le résultat obtenu
     */
    public static String transformArrayToCSVString(Object[] data){
        StringBuilder lineValues  = new StringBuilder();

        for(int i = 0; i < data.length; i++){
            lineValues.append(String.valueOf(data[i]));
            //Le séparateur
            if(i != (data.length - 1)) lineValues.append(";");
        }
        return lineValues.toString();
    }
    
	/**
     * Retourne le nom d'un fichier sans son extension
     * @param filename Le nom du fichier original 
     * @return Le résultat obtenu
     */
    public static String filenameWithoutExtension(String filename){
        if (filename == null) return null;

        int positon = filename.lastIndexOf(".");
        if (positon == -1) return filename;
        return filename.substring(0, positon);
    }
}
