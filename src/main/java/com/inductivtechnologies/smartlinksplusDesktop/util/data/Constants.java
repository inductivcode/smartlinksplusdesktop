package com.inductivtechnologies.smartlinksplusDesktop.util.data;

/**
 * Constants : Regroupe quelques constantes
 */
public class Constants {
	
	//Nom des fichiers lors de l'extraction des données 
	public static final String CSV_DATA_FILE_NAME = "%s_Historical_data.csv";
	public static final String CSV_COMPANY_FILE_NAME = "Companies.csv";
	public static final String CSV_FXSPOT_SYMBOL_FILE_NAME = "FxSpot_symbols.csv";

	public static final String TOKEN_SCHEME = "auth-token";
	
	public static final String DATETIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
	
	// Pour afficher les messages lors de la mise à jour des données 
	public static final String US_DATETIME_FORMAT = "YYYY-MM-dd HH:MM:ss";
	
	// Pour afficher la date des données 
	public static final String PRINT_DATE_FORMAT = "dd-MM-YYYY";
	
	public static final String[] MONTH_IN_ENGLISH = { "Jan", "Feb", "Mar", "Apr", 
            "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec"
          };
	
	//On se sert de ce format pour enregistrer les date dans la base de données
	public static final String DATA_FORMAT_DATE = "YYYY-MM-dd";
	
	public static final String EQUITY_TYPE = "EQUITY";
	public static final String FXSPOT_TYPE = "FX_SPOT";
	public static final String COMPANY_TYPE = "COMPANY";
	public static final String FXSPOT_SYMBOLS_TYPE = "FXSPOT_SYMBOLS";
	
	//Colonnes des symboles fx spot 
	public static final String[] FXSPOT_SYMBOL_CSV_COLUMNS = { "Symbole", "Description du symbole"};
	//Colonnes des compagnies
	public static final String[] COMPANY_CSV_COLUMNS = { "Symbole", "Nom", "Secteur", "Industrie", "Description"};
	//Les colonnes du fichier csv contenant les FXSPOT
	public static final String[] FXSPOT_CSV_COLUMNS = { "Date", "Price", "Open", "High", "Low", "change %"};
	//Les colonnes du fichier csv contenant les EQUITY
	public static final String[] EQUITY_CSV_COLUMNS = { "Date", "Open", "High", "Low", "Close", "Adj", "Volume"};
	
    //Les commandes
    public static final String BTN_EXPORT_DATA_CM = "Exporte les données";

    public static final String BTN_COMPANIES_CM = "Charge les compagnies";
    public static final String BTN_FXSPOT_SYMBOLS_CM = "Charge les symbole fxspot";
    public static final String BTN_EQUITIES_CM = "Charge les equities";
    public static final String BTN_FXSPOT_CM = "Charge les fxspot";
}
