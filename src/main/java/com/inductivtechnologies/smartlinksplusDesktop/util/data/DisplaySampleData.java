package com.inductivtechnologies.smartlinksplusDesktop.util.data;

import java.util.List;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.Company;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;
import com.inductivtechnologies.smartlinksplusDesktop.ui.tools.WhatIDo;
import com.inductivtechnologies.smartlinksplusDesktop.util.http.BuildGson;

/**
 * DisplaySampleData : Permet d'afficheer quelques POJO dans la console 
 */
public class DisplaySampleData {
	
	public static final String TAG = DisplaySampleData.class.getSimpleName();
	
	/**
	 * displayJSON : Affiche un objet au format JSON intélligent
	 */
	public static <T> void displayJSON(T object) {
		Gson gson = BuildGson.buildPrettyGson();
		String json = gson.toJson(object);
		
		System.out.println(json);
	}

	/**
     * Affiche plusieurs fx spot symbols
     * @param fxSpotSymbols Les symboles à afficher
     */
	public static void viewFxSpotSymbols(List<FxSpotSymbol> fxSpotSymbols){
		if(!fxSpotSymbols.isEmpty()){
			for(FxSpotSymbol fxSpotSymbol : fxSpotSymbols){
				viewFxSpotSymbol(fxSpotSymbol);
			}
		}else{
			WhatIDo.message(TAG, "Aucune donnée");
		}
	}
    
	/**
     * Affiche un fx spot symbol 
     * @param fxSpotSymbol Le symbol à afficher
     */
	public static void viewFxSpotSymbol(FxSpotSymbol fxSpotSymbol){
		System.out.print("Id : " + fxSpotSymbol.getFxSpotSymbolId());
		System.out.print(" / Symbole : " + fxSpotSymbol.getSymbolValue());
		System.out.print(" / Description : " + fxSpotSymbol.getSymbolDescription());
		System.out.print(" / Date de création : " +
				DateUtils.printJavaDate(fxSpotSymbol.getCreationDate(), 
								Constants.PRINT_DATE_FORMAT));
		
		System.out.println("");
	}
	
	/**
     * Affiche un marché
     * @param market Le marché à afficher
     */
	public static void viewMarket(RawMarketData market){
		System.out.print("Id : " + market.getIdRawMarketData());
		System.out.print("Date : " +
				DateUtils.printJavaDate(market.getRetrievedDate(), 
								Constants.PRINT_DATE_FORMAT));
		System.out.print(" / Symbole : " + market.getSymbol());
		System.out.print(" / Type : " + market.getType());
		System.out.print(" / Open : " + market.getOpenedValue());
		System.out.print(" / Close : " + market.getClosedValue());
		System.out.print(" / High : " + market.getHighestValue());
		System.out.print(" / Low : " + market.getLowestValue());
		System.out.print(" / Change : " + market.getChangeValue());
		
		System.out.println("");
	}
	
	/**
     * Affiche quelques marchés
     * @param markets Les marchés à afficher
     */
	public static void viewMarkets(List<RawMarketData> markets){
		if(!markets.isEmpty()){
			for(RawMarketData market : markets){
				viewMarket(market);
			}
		}else{
			WhatIDo.message(TAG, "Aucune donnée");
		}
	}
	
	/**
     * Affiche une compagnie
     * @param company La compagnie à afficher
     */
	public static void viewCompany(Company company){
		System.out.print("Id : " + company.getIdCompany());
		System.out.print(" / Symbole : " + company.getSymbol());
		System.out.print(" / Nom : " + company.getName());
		
		if(company.getProfile() != null) {
			System.out.print(" / Secteur : " + company.getProfile().getSector());
			System.out.print(" / Industrie : " + company.getProfile().getIndustry());
			System.out.print(" / Nombe d'mployé : " + company.getProfile().getNbEmp());
		}
	
		System.out.println("");
	}
	
	/**
     * Affiche quelques compagnies
     * @param companies Les compagnies à afficher
     */
	public static void viewCompanies(List<Company> companies){
		if(!companies.isEmpty()){
			for(Company company : companies){
				viewCompany(company);
			}
		}else{
			WhatIDo.message(TAG, "Aucune donnée");
		}
	}
	
}
