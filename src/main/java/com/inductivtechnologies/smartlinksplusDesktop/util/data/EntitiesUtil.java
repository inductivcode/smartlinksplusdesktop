package com.inductivtechnologies.smartlinksplusDesktop.util.data;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.inductivtechnologies.smartlinksplusDesktop.models.market.Company;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.Financial;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.FxSpotSymbol;
import com.inductivtechnologies.smartlinksplusDesktop.models.market.RawMarketData;

/**
 * EntitiesUtil : Collection de méthodes utiles pour les entités
 */
public class EntitiesUtil {

	/**
	 * Crée un ObjectId à partir d'un objet JSON
	 * @param jsonObject JSON
	 * @return ObjectId
	 */
    public static ObjectId getObjectIdByJSONObject(JSONObject jsonObject){
        ObjectId objectId = null;
        if(jsonObject != null){
            try{
                objectId = new ObjectId(jsonObject.getInt("timestamp"),
                		jsonObject.getInt("machineIdentifier"),
                		(short)jsonObject.getInt("processIdentifier"),
                         jsonObject.getInt("counter"));
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
        }
        
        return objectId;
    }
    
	/**
	 * Représentation hexadécimale d'un objectId
	 * @param jsonObject JSON
	 * @return String Forme hexa
	 */
    public static String getObjectIdHexByJSONObject(JSONObject jsonObject){
        return getObjectIdByJSONObject(jsonObject).toString();
    }
    
	/**
	 * Crée une liste d'objets FxSpotSymbol à partir d'un objet JSON Array
	 * @param jsonObject JSON
	 * @throws JSONException
	 * @return La liste obtenue
	 */
    public static List<FxSpotSymbol> getListFxSpotSymbolByJSONArray(JSONArray jsonArray) throws JSONException{
        List<FxSpotSymbol> symbols = new ArrayList<>();
        
        if(jsonArray != null && jsonArray.length() != 0){
        	for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonData = jsonArray.getJSONObject(i);

                FxSpotSymbol symbol = new FxSpotSymbol(jsonData);
                symbols.add(symbol);
        	}
        }
        
        return symbols;
    }
    
	/**
	 * Crée une liste d'objets RawMarketData à partir d'un objet JSON Array
	 * @param jsonArray JSON
	 * @throws JSONException
	 * @return La liste obtenue
	 */
    public static List<RawMarketData> getListRawMarketDataByJSONArray(JSONArray jsonArray) throws JSONException{
        List<RawMarketData> markets = new ArrayList<>();
        
        if(jsonArray != null && jsonArray.length() != 0){
        	for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonData = jsonArray.getJSONObject(i);

                RawMarketData market = new RawMarketData(jsonData);
                markets.add(market);
        	}
        }
        
        return markets;
    }
    
	/**
	 * Crée une liste d'objets Company à partir d'un objet JSON Array
	 * @param jsonArray JSON
	 * @throws JSONException
	 * @return La liste obtenue
	 */
    public static List<Company> getListCompaniesByJSONArray(JSONArray jsonArray) throws JSONException{
        List<Company> companies = new ArrayList<>();
        
        if(jsonArray != null && jsonArray.length() != 0){
        	for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonData = jsonArray.getJSONObject(i);

                Company company = new Company(jsonData);
                companies.add(company);
        	}
        }
        
        return companies;
    }
    
	/**
	 * Crée une liste d'objets Financial à partir d'un objet JSON Array
	 * @param jsonArray JSON
	 * @throws JSONException
	 * @return La liste obtenue
	 */
    public static List<Financial> getListFinancialsByJSONArray(JSONArray jsonArray) throws JSONException{
        List<Financial> financials = new ArrayList<>();
        
        if(jsonArray != null && jsonArray.length() != 0){
        	for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonData = jsonArray.getJSONObject(i);

                Financial financial = new Financial(jsonData);
                financials.add(financial);
        	}
        }
        
        return financials;
    }
    
	/**
     * Converti une liste d'equities en tableau 2D
     * @param data Liste à convertir 
     * @return Résultat de la convertion
     */
    public static Object[][] convertListEquitiesTo2DObject(List<RawMarketData> data){
        Object[][] dataTable = new Object[data.size()][7];

        for(int i=0; i < data.size(); i++){
            Object[] objects = new Object[7];

            objects[0] = DateUtils.printJavaDate(data.get(i).getRetrievedDate(), Constants.PRINT_DATE_FORMAT);
            objects[1] = data.get(i).getOpenedValue();
            objects[2] = data.get(i).getHighestValue();
            objects[3] = data.get(i).getLowestValue();
            objects[4] = data.get(i).getClosedValue();
            objects[5] = data.get(i).getAdjclose();
            objects[6] = data.get(i).getVolume();

            dataTable[i] = objects;
        }

        return dataTable;
    }
    
	/**
     * Converti une liste de FXSPOT en tableau 2D
     * @param data Liste à convertir 
     * @return Résultat de la convertion
     */
    public static Object[][] convertListFxSpotTo2DObject(List<RawMarketData> data){
        Object[][] dataTable = new Object[data.size()][6];

        for(int i=0; i < data.size(); i++){
            Object[] objects = new Object[6];

            objects[0] = DateUtils.printJavaDate(data.get(i).getRetrievedDate(), Constants.PRINT_DATE_FORMAT);
            objects[1] = data.get(i).getClosedValue();
            objects[2] = data.get(i).getOpenedValue();
            objects[3] = data.get(i).getHighestValue();
            objects[4] = data.get(i).getLowestValue();
            objects[5] = data.get(i).getChangeValue();

            dataTable[i] = objects;
        }

        return dataTable;
    }
    
	/**
     * Converti une liste de marchés en tableau 2D
     * @param data Liste à convertir 
     * @return Résultat de la convertion
     */
    public static Object[][] convertListCompanyTo2DObject(List<Company> companies){
        Object[][] dataTable = new Object[companies.size()][5];

        for(int i=0; i < companies.size(); i++){
            Object[] objects = new Object[5];

            objects[0] = companies.get(i).getSymbol();
            objects[1] = companies.get(i).getName();
            
            objects[2] = companies.get(i).getProfile() == null ? "" : companies.get(i).getProfile().getSector();
            objects[3] = companies.get(i).getProfile() == null ? "" : companies.get(i).getProfile().getIndustry();
            objects[4] = companies.get(i).getProfile() == null ? "" : companies.get(i).getProfile().getDescription();

            dataTable[i] = objects;
        }

        return dataTable;
    }
    
	/**
     * Converti une liste de marchés en tableau 2D
     * @param data Liste à convertir 
     * @return Résultat de la convertion
     */
    public static Object[][] convertListFxSpotSymbolTo2DObject(List<FxSpotSymbol> symbols){
        Object[][] dataTable = new Object[symbols.size()][2];

        for(int i=0; i < symbols.size(); i++){
            Object[] objects = new Object[2];

            objects[0] = symbols.get(i).getSymbolValue();
            objects[1] = symbols.get(i).getSymbolDescription();

            dataTable[i] = objects;
        }

        return dataTable;
    }
	
}
