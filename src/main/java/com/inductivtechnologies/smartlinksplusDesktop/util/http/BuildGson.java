package com.inductivtechnologies.smartlinksplusDesktop.util.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Constants;

/**
 * BuildGson : Regroupe quelques méthodes qui permettent de contruire des objets GSON 
 */
public class BuildGson {

	/**
	 * Construit un objet Gson simple
	 * @return L'objet Gson construit
	 */
	public static Gson buildSimpleGson(){
		return new GsonBuilder().setLenient().create();
	}
	
	/**
	 * Construit un objet Gson pour l'affichage
	 * @return L'objet Gson construit
	 */
	public static Gson buildPrettyGson(){
		return new GsonBuilder().setPrettyPrinting().create();
	}
	
	/**
	 * Construit un objet Gson en configurant la date 
	 * @return L'objet Gson construit
	 */
	public static Gson buildGsonWithCustomDate(){
		Gson gsonObject = new GsonBuilder()
				.setDateFormat(Constants.DATETIME_FORMAT)
				.create();
		
		return gsonObject;
	}
	
}
