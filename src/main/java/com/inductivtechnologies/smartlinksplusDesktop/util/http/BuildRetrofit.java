package com.inductivtechnologies.smartlinksplusDesktop.util.http;

import java.io.IOException;

import com.google.gson.Gson;
import com.inductivtechnologies.smartlinksplusDesktop.config.EndPointsConfig;
import com.inductivtechnologies.smartlinksplusDesktop.util.data.Constants;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.Retrofit.Builder;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * BuildRetrofit : Regroupe quelques méthodes qui permettent de contruire des objets Retrofit 
 */
public class BuildRetrofit {
	
	/**
	 * Génère un objet OkHttpClient pour réaliser une  authentification et autorisation 
	 *  @param tokenValue La valeur du token 
	 *  @return OkHttpClient
	 */
	private static OkHttpClient buildOkHttpClientWithToken(final String tokenValue) {
		OkHttpClient okHttpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
			
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request originalRequest = chain.request();

                Request.Builder builder = originalRequest
                		.newBuilder().header(Constants.TOKEN_SCHEME, tokenValue);

                Request newRequest = builder.build();
                return chain.proceed(newRequest);
            }
        }).build();
		
		return okHttpClient;
	}

	/**
	 * Génère un objet Retrofit pré construit simple
	 * @param gson Le convertisseur JSON-Object
	 * @return L'objet Retrofit pré-construit (Il faut faire builder.build)
	 */
	public static Builder simpleRetrofitBuilder(Gson gsonObject){
		Builder retrofitBuilder = new Retrofit.Builder()
		        .baseUrl(EndPointsConfig.API_BASE_URL)
		        .addConverterFactory(GsonConverterFactory.create(gsonObject));
		
		return retrofitBuilder;
	}
	
	/**
	 * Génère un objet Retrofit pré construit pour des requêtes avec autorisation par token 
	 * @param gson Le convertisseur JSON-Object
	 * @param tokenValue La valeur du token 
	 * @return L'objet Retrofit pré-construit (Il faut faire builder.build)
	 */
	public static Builder authTokenRetrofitBuilder(Gson gsonObject, final String tokenValue){
		Builder retrofitBuilder = new Retrofit.Builder()
		        .baseUrl(EndPointsConfig.API_BASE_URL)
		        .client(buildOkHttpClientWithToken(tokenValue))
		        .addConverterFactory(GsonConverterFactory.create(gsonObject));
		
		return retrofitBuilder;
	}
	
}
