package com.inductivtechnologies.smartlinksplusDesktop.util.http;

import com.inductivtechnologies.smartlinksplusDesktop.services.account.TokenService;
import com.inductivtechnologies.smartlinksplusDesktop.services.account.UserService;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.CompanyService;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.FxSpotSymbolService;
import com.inductivtechnologies.smartlinksplusDesktop.services.market.RawMarketDataService;

import retrofit2.Retrofit;

/**
 * BuildServiceInstance : Fournie une instance des différents services 
 */
public class BuildServiceInstance {

	/**
	 * UserServiceInstance : Fournie une instance du service UserService
	 * @param retrofit POur faire les appels
	 * @result UserService
	 */
	public static UserService UserServiceInstance(Retrofit retrofit) {
		return retrofit.create(UserService.class);
	}
	
	/**
	 * fxspotSymbolServiceInstance : Fournie une instance du service FxSpotSymbolService
	 * @param retrofit POur faire les appels
	 * @result FxSpotSymbolService
	 */
	public static FxSpotSymbolService fxspotSymbolServiceInstance(Retrofit retrofit) {
		return retrofit.create(FxSpotSymbolService.class);
	}
	
	/**
	 * rawMarketDataServiceInstance : Fournie une instance du service RawMarketDataServiceInstance
	 * @param retrofit POur faire les appels
	 * @result RawMarketDataService
	 */
	public static RawMarketDataService rawMarketDataServiceInstance(Retrofit retrofit) {
		return retrofit.create(RawMarketDataService.class);
	}
	
	/**
	 * companyServiceInstance : Fournie une instance du service CompanyService
	 * @param retrofit POur faire les appels
	 * @result CompanyService
	 */
	public static CompanyService companyServiceInstance(Retrofit retrofit) {
		return retrofit.create(CompanyService.class);
	}
	
	/**
	 * tokenServiceInstance : Fournie une instance du service TokenService
	 * @param retrofit POur faire les appels
	 * @result TokenService
	 */
	public static TokenService tokenServiceInstance(Retrofit retrofit) {
		return retrofit.create(TokenService.class);
	}
}
