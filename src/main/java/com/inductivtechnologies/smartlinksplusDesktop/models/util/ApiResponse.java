package com.inductivtechnologies.smartlinksplusDesktop.models.util;

import java.io.Serializable;
import java.util.Map;

/**
 * ApiResponse : Utilisée pour récupérer les réponses données par l'api 
*/
public class ApiResponse implements Serializable {
	
	/**
	 * Pour la sérialisation 
	 */
	private static final long serialVersionUID = -8663839363716586260L;
	
	//Les codes 
	//Requête traitée avec succès et présence de données
	public static final int CODE_SUCCESS = 1000;
	//Requête traitée avec succès mais pas de données
	public static final int CODE_SUCCESS_NO_DATA = 1010;
	//Requête  non traitée
	public static final int CODE_ERROR = 2000;
	//Requête  non traitée date non valide
	public static final int CODE_INVALID_FORMAT_DATE = 2010;
	//Requête  non traitée (mauvaise requête)
	public static final int CODE_INVALID_REQUEST = 2020;
	//Authentification échouée
	public static final int CODE_AUTHENTIFICATION_FAILED = 2040;
	//Authentification échouée token expiré 
	public static final int CODE_TOKEN_EXPIRED = 2050;
	//Authentification échouée le token n'existe pas  
	public static final int CODE_TOKEN_UNEXIST = 2060;
	
	public static final String STR_MESSAGE = "message";
	public static final String STR_CODE = "code";

	/**
	 * code : Code de réponse 
	*/
	private int code;
	
	/**
	 * message : Message de notifications 
	*/
	private String message;
	
	/**
	 * dataCount : Le nombre de résultats 
	*/
	private long dataCount;
	
	/**
	 * data : Les données
	*/
	private Map<String, Object> data;
	
	/**
	 * Constructeur
	*/
	public ApiResponse() {
		super();
		this.dataCount = 0;
	}
	
	/**
	 * Constructeur
	*/
	public ApiResponse(int code, String message, int dataCount, Map<String, Object> data) {
		super();
		this.code = code;
		this.message = message;
		this.dataCount = dataCount;
		this.data = data;
	}

	//Getters et setters
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public long getDataCount() {
		return dataCount;
	}

	public void setDataCount(long dataCount) {
		this.dataCount = dataCount;
	}
	
}
