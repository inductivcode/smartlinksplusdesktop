package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;

/**
 * Company :La compagnie
*/
public class Company implements Serializable{

	public static final String COMPANY_DATA_NAME = "company";
	public static final String COMPANY_DATA_NAME_PLU = "companies";
	
	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = -3100742562865567157L;

	/**
	 * L'id de la compagnie
	 */
	private String idCompany;
	
	/**
	 * Le nom de la compagnie
	 */
	private String name;

	/**
	 * Le symbole de la compagnie
	 */
	private String symbol;
	
	/**
	 * Le profile de la compagnie
	 */
	private ProfileComp profile;
	
	/**
	 * Outlook de la compagnie
	 */
	private OutLook outlook;
	
	/**
	 * Financial de la companie
	 */
	private List<Financial> financial;
	
	/**
	 * Constructeur
	 */
	public Company() {
		super();
		this.financial = new ArrayList<Financial>();
	}

	/**
	 * Constructeur
	*/
	public Company(String idCompany, String name, String symbol,
			ProfileComp profile, OutLook outlook,
			List<Financial> financial){
		super();
		this.idCompany = idCompany;
		this.name = name;
		this.symbol = symbol;
		this.profile = profile;
		this.outlook = outlook;
		this.financial = financial;
	}

	
	/**
	 * Constructeur
	*/
	public Company(JSONObject jsonObject) throws JSONException{
		super();
		
		this.idCompany = jsonObject.isNull("idCompany") ? "" :
			EntitiesUtil  .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idCompany"));
		
		this.name = jsonObject.isNull("name") ? "" : jsonObject.getString("name");
		this.symbol = jsonObject.isNull("symbol") ? "" : jsonObject.getString("symbol")
				;
		this.profile = jsonObject.isNull("profile") ? null : new ProfileComp(jsonObject.getJSONObject("profile"));
		this.outlook = jsonObject.isNull("outlook") ? null : new OutLook(jsonObject.getJSONObject("outlook"));
		
		this.financial = jsonObject.isNull("financial") ? null : 
			EntitiesUtil.getListFinancialsByJSONArray(jsonObject.getJSONArray("financial"));
	}
	
	//Getters et setters 
	
	public List<Financial> getFinancial() {
		return financial;
	}

	public void setFinancial(List<Financial> financial) {
		this.financial = financial;
	}

	public String getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(String idCompany) {
		this.idCompany = idCompany;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public ProfileComp getProfile() {
		return profile;
	}

	public void setProfile(ProfileComp profile) {
		this.profile = profile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public OutLook getOutlook() {
		return outlook;
	}

	public void setOutlook(OutLook outlook) {
		this.outlook = outlook;
	}
}