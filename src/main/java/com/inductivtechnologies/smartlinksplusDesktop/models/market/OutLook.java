package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * OutLook : outlook de la compagnie
 **/
public class OutLook implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 1585295307591802639L;

	/**
	 * Evènement récent 
	 */
	private String event;
	
	/**
	 * Evènement suivant 
	 */
	private String nextEvent;

	/**
	 * Constructeur
	 */
	public OutLook() {
		super();
	}

	/**
	 * Constructeur
	 */
	public OutLook(String event, String nextEvent) {
		super();
		this.event = event;
		this.nextEvent = nextEvent;
	}
	
	/**
	 * Constructeur
	 */
	public OutLook(JSONObject jsonObject) throws JSONException{
		super();
		
		this.event = jsonObject.isNull("event") ? "" : jsonObject.getString("event");
		this.nextEvent = jsonObject.isNull("nextEvent") ? "" : jsonObject.getString("nextEvent");
	}

	public String getEvent() {
		return event;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}

	public String getNextEvent() {
		return nextEvent;
	}

	public void setNextEvent(String nextEvent) {
		this.nextEvent = nextEvent;
	}
}
