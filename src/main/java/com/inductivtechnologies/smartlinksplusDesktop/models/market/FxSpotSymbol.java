package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;

/**
 * FxSpotSymbol : Englobe les données relatives aux symboles des Fx Spot 
 */
public class FxSpotSymbol implements Serializable{
	
	public static final String FXSPOTSYMBOL_DATA_NAME = "fxspot_symbols";
	public static final String FXSPOTSYMBOL_DATA_NAME_SIN = "fxspot_symbol";

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 2533507189021663830L;

	/**
	 * L'id unique 
	 */
	private String fxSpotSymbolId;
	
	/**
	 * La valeur du symbole Ex EURUSD
	 */
	private String symbolValue;
	
	/**
	 * La description du symbole Ex pour EURUSD : Euro US Dollar
	 */
	private String symbolDescription;
	
	/**
	 * Date de création
	 */
	private final Date creationDate;
	
	/**
	 * Constructeur
	 */
	public FxSpotSymbol(){
		super();
		this.creationDate = new Date();
	}
	
	/**
	 * Constructeur
	 */
	public FxSpotSymbol(String fxSpotSymbolId, String symbolValue,
			String symbolDescription, Date creationDate) {
		super();
		this.fxSpotSymbolId = fxSpotSymbolId;
		this.symbolValue = symbolValue;
		this.symbolDescription = symbolDescription;
		this.creationDate = creationDate;
	}
	
	/**
	 * Constructeur
	 */
	public FxSpotSymbol(JSONObject jsonObject) throws JSONException{
		super();
		
		this.fxSpotSymbolId = jsonObject.isNull("fxSpotSymbolId") ? "" : 
			EntitiesUtil.getObjectIdHexByJSONObject(jsonObject.getJSONObject("fxSpotSymbolId"));
		
		this.symbolValue = jsonObject.isNull("symbolValue") ? "" :
			jsonObject.getString("symbolValue");
		this.symbolDescription = jsonObject.isNull("symbolDescription") ? "" : 
			jsonObject.getString("symbolDescription");
		
	    this.creationDate = jsonObject.isNull("creationDate") ? null :
         	new Date(jsonObject.getLong("creationDate"));
	}

	//Getters et setters 
	
	public String getFxSpotSymbolId() {
		return fxSpotSymbolId;
	}

	public void setFxSpotSymbolId(String fxSpotSymbolId) {
		this.fxSpotSymbolId = fxSpotSymbolId;
	}

	public String getSymbolValue() {
		return symbolValue;
	}

	public void setSymbolValue(String symbolValue) {
		this.symbolValue = symbolValue;
	}

	public String getSymbolDescription() {
		return symbolDescription;
	}

	public void setSymbolDescription(String symbolDescription) {
		this.symbolDescription = symbolDescription;
	}

	public Date getCreationDate() {
		return creationDate;
	}
}
