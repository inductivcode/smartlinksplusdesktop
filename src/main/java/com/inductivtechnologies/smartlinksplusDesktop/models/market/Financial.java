package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Financial: les états financiers de la compagnie
 **/
public class Financial implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 6107297565571143913L;
	
	/**
	 * turnover
	 */
	private Double turnover;
	
	/**
	 * operatingProfit
	 */
	private Double operatingProfit;
	
	/**
	 * netProfit
	 */
	private Double netProfit;	
	
	/**
	 * reportedEPS
	 */
	private Double reportedEPS;
	
	/**
	 * currentAssets
	 */
	private Double currentAssets;	
	
	/**
	 * nonCurrentAssets
	 */
	private Double nonCurrentAssets;	
	
	/**
	 * totalAssets
	 */
	private Double totalAssets;	
	
	/**
	 *currentLiabilities
	 */
	private Double currentLiabilities;	
	
	/**
	 * totalLiabilities
	 */
	private Double totalLiabilities;	
	
	/**
	 * totalEquity
	 */
	private Double totalEquity;	
	
	/**
	 * operatingCashFlow
	 */
	private Double operatingCashFlow;	
	
	/**
	 * netChangeinCash
	 */
	private Double netChangeinCash;
	
	/**
	 * category
	 */
	private String category;
	
	/**
	 * year
	 */
	private String year;

	/**
	 * Constructeur
	 */
	public Financial() {
		super();
	}

	/**
	 * Constructeur
	 */
	public Financial(Double turnover, Double operatingProfit, Double netProfit, Double reportedEPS,
			Double currentAssets, Double nonCurrentAssets, Double totalAssets, Double currentLiabilities,
			Double totalLiabilities, Double totalEquity, Double operatingCashFlow, Double netChangeinCash,
			String category, String year) {
		
		super();
		this.turnover = turnover;
		this.operatingProfit = operatingProfit;
		this.netProfit = netProfit;
		this.reportedEPS = reportedEPS;
		this.currentAssets = currentAssets;
		this.nonCurrentAssets = nonCurrentAssets;
		this.totalAssets = totalAssets;
		this.currentLiabilities = currentLiabilities;
		this.totalLiabilities = totalLiabilities;
		this.totalEquity = totalEquity;
		this.operatingCashFlow = operatingCashFlow;
		this.netChangeinCash = netChangeinCash;
		this.category = category;
		this.year = year;
	}
	
	/**
	 * Constructeur
	 */
	public Financial(JSONObject jsonObject) throws JSONException {
		
		super();
		
		this.turnover = jsonObject.isNull("turnover") ? null : jsonObject.getDouble("turnover");
		this.operatingProfit = jsonObject.isNull("operatingProfit") ? null : jsonObject.getDouble("operatingProfit");
		this.netProfit = jsonObject.isNull("netProfit") ? null : jsonObject.getDouble("netProfit");
		this.reportedEPS = jsonObject.isNull("reportedEPS") ? null : jsonObject.getDouble("reportedEPS");
		
		this.currentAssets = jsonObject.isNull("currentAssets") ? null : jsonObject.getDouble("currentAssets");
		this.nonCurrentAssets = jsonObject.isNull("nonCurrentAssets") ? null : jsonObject.getDouble("nonCurrentAssets");
		this.totalAssets = jsonObject.isNull("totalAssets") ? null : jsonObject.getDouble("totalAssets");
		this.currentLiabilities = jsonObject.isNull("currentLiabilities") ? null : jsonObject.getDouble("currentLiabilities");
		
		this.totalLiabilities = jsonObject.isNull("totalLiabilities") ? null : jsonObject.getDouble("totalLiabilities");
		this.totalEquity = jsonObject.isNull("totalEquity") ? null : jsonObject.getDouble("totalEquity");
		this.operatingCashFlow = jsonObject.isNull("operatingCashFlow") ? null : jsonObject.getDouble("operatingCashFlow");
		this.netChangeinCash = jsonObject.isNull("netChangeinCash") ? null : jsonObject.getDouble("netChangeinCash");
		
		this.category = jsonObject.isNull("category") ? "" : jsonObject.getString("category");
		this.year = jsonObject.isNull("year") ? "" : jsonObject.getString("year");
	}
	
	//Getters et setters 
	
	public Double getTurnover() {
		return turnover;
	}

	public void setTurnover(Double turnover) {
		this.turnover = turnover;
	}

	public Double getOperatingProfit() {
		return operatingProfit;
	}

	public void setOperatingProfit(Double operatingProfit) {
		this.operatingProfit = operatingProfit;
	}

	public Double getNetProfit() {
		return netProfit;
	}

	public void setNetProfit(Double netProfit) {
		this.netProfit = netProfit;
	}

	public Double getReportedEPS() {
		return reportedEPS;
	}

	public void setReportedEPS(Double reportedEPS) {
		this.reportedEPS = reportedEPS;
	}

	public Double getCurrentAssets() {
		return currentAssets;
	}

	public void setCurrentAssets(Double currentAssets) {
		this.currentAssets = currentAssets;
	}

	public Double getNonCurrentAssets() {
		return nonCurrentAssets;
	}

	public void setNonCurrentAssets(Double nonCurrentAssets) {
		this.nonCurrentAssets = nonCurrentAssets;
	}

	public Double getTotalAssets() {
		return totalAssets;
	}

	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}

	public Double getCurrentLiabilities() {
		return currentLiabilities;
	}

	public void setCurrentLiabilities(Double currentLiabilities) {
		this.currentLiabilities = currentLiabilities;
	}

	public Double getTotalLiabilities() {
		return totalLiabilities;
	}

	public void setTotalLiabilities(Double totalLiabilities) {
		this.totalLiabilities = totalLiabilities;
	}

	public Double getTotalEquity() {
		return totalEquity;
	}

	public void setTotalEquity(Double totalEquity) {
		this.totalEquity = totalEquity;
	}

	public Double getOperatingCashFlow() {
		return operatingCashFlow;
	}

	public void setOperatingCashFlow(Double operatingCashFlow) {
		this.operatingCashFlow = operatingCashFlow;
	}

	public Double getNetChangeinCash() {
		return netChangeinCash;
	}

	public void setNetChangeinCash(Double netChangeinCash) {
		this.netChangeinCash = netChangeinCash;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
}