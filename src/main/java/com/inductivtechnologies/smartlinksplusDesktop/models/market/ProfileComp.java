package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * ProfileComp : Le profile de la compagnie
 **/
public class ProfileComp implements Serializable{

	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 5600227859428656864L;

	/**
	 * Le secteur d'activité
	 */
    private String sector;
	
    /**
	 * Le type d'industrie
	 */
    private String industry;
    
    /**
	 *Indique comment les actions de la compagnie sont stoquées
	 */
    private String stockStyle;
    
    /**
	 * Le nombre d'employé
	 */
    private Double nbEmp;
    
    /**
	 * fiscalYearEnds
	 */
    private String fiscalYearEnds;
    
    /**
     * Description de la compagnie
     */
    private String description;

	/**
	 * Constructeur
	*/
	public ProfileComp() {
		super();
	}

	/**
	 * Constructeur
	*/
	public ProfileComp(String sector, String industry, String stockStyle,
			Double nbEmp, String fiscalYearEnds,
			String description) {
		super();
		this.sector = sector;
		this.industry = industry;
		this.stockStyle = stockStyle;
		this.nbEmp = nbEmp;
		this.fiscalYearEnds = fiscalYearEnds;
		this.description = description;
	}

	/**
	 * Constructeur
	*/
	public ProfileComp(JSONObject jsonObject) throws JSONException{
		super();
		
		this.sector = jsonObject.isNull("sector") ? "" : jsonObject.getString("sector");
		this.industry = jsonObject.isNull("industry") ? "" : jsonObject.getString("industry");
		this.stockStyle = jsonObject.isNull("stockStyle") ? "" : jsonObject.getString("stockStyle");
		
		this.nbEmp = jsonObject.isNull("nbEmp") ? null : jsonObject.getDouble("nbEmp");
		
		this.fiscalYearEnds = jsonObject.isNull("stockStyle") ? "" : jsonObject.getString("stockStyle");
		this.description = jsonObject.isNull("description") ? "" : jsonObject.getString("description");
	}
	
	//Getters et setters 
	
	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getStockStyle() {
		return stockStyle;
	}

	public void setStockStyle(String stockStyle) {
		this.stockStyle = stockStyle;
	}

	public Double getNbEmp() {
		return nbEmp;
	}

	public void setNbEmp(Double nbEmp) {
		this.nbEmp = nbEmp;
	}

	public String getFiscalYearEnds() {
		return fiscalYearEnds;
	}

	public void setFiscalYearEnds(String fiscalYearEnds) {
		this.fiscalYearEnds = fiscalYearEnds;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}