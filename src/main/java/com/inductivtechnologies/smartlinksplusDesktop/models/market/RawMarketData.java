package com.inductivtechnologies.smartlinksplusDesktop.models.market;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;

/**
 * RawMarketData : Englobe les données d'un marché 
 */
public class RawMarketData implements Serializable {
	
	/**
	 * Pour la sérialisation
	 */
	private static final long serialVersionUID = 6554761599262413040L;

	/**
	 * L'id du RawMarketData courant
	 */
	private String idRawMarketData;
	
	/**
	 * Le symbole de l'entreprise
	 */
	private String symbol;
	
	/**
	 * Prix du produit financier à l'ouverture du marché financier
	 */
	private Double openedValue;
	
	/**
	 * Le prix d'un produit financier à la fermeture d'un marché financier
	 */
	private Double closedValue;

	/**
	 * Le prix le plus bas d'un produit financier dans une journée
	 */
	private Double lowestValue;

	/**
	 * Le prix le plus haut d'un produit financier dans une journée
	 */
	private Double highestValue;

	/**
	 * Le dernier prix d'un produit financier sur un marché financier
	 */
	private Double lastValue;
	
	/**
	 *  Le nombre d'actions qui ont été négociées, achetées ou vendues tout au long de la journée
	 */
	private Double volume;

	/**
	 * Prix que les gens sont prêts à payer pour avoir le produit financier
	 */
	private Double askedValue;
	
	/**
	 * Prix que les gens sont prêts à payer pour avoir le produit financier
	 */
	private Double adjclose;
	
	/**
	 * changeValue en pourcentage 
	 */
	private String changeValue;
	
	/**
	 * La date à laquelle les prix sont récupérés
	 */
	private Date retrievedDate;
	
	/**
	 * Le type du marché
	 */
	private String type;
	
	/**
	 * Date de création du RawMarketData
	 */
	private final Date creationDate;

	/**
	 * Constructeur
	 */
	public RawMarketData() {
		super();
		creationDate = new Date();
	}
	
	/**
	 * Constructeur
	 */
	public RawMarketData(String idRawMarketData, String symbol, Double openedValue, Double closedValue,
			Double lowestValue, Double highestValue, Double lastValue, Double volume, Double askedValue,
			Double adjclose, String changeValue, Date retrievedDate, String type, Date creationDate) {
		super();
		this.idRawMarketData = idRawMarketData;
		this.symbol = symbol;
		this.openedValue = openedValue;
		this.closedValue = closedValue;
		this.lowestValue = lowestValue;
		this.highestValue = highestValue;
		this.lastValue = lastValue;
		this.volume = volume;
		this.askedValue = askedValue;
		this.adjclose = adjclose;
		this.changeValue = changeValue;
		this.retrievedDate = retrievedDate;
		this.type = type;
		this.creationDate = creationDate;
	}

	/**
	 * Constructeur
	 */
	public RawMarketData(JSONObject jsonObject) throws JSONException{
		super();
		
		this.idRawMarketData = jsonObject.isNull("idRawMarketData") ? "" : 
			EntitiesUtil.getObjectIdHexByJSONObject(jsonObject.getJSONObject("idRawMarketData"));
			
		this.symbol = jsonObject.isNull("symbol") ? "" : jsonObject.getString("symbol");
			
		this.openedValue = jsonObject.isNull("openedValue") ? null : jsonObject.getDouble("openedValue");
		this.closedValue = jsonObject.isNull("closedValue") ? null : jsonObject.getDouble("closedValue");
		this.lowestValue = jsonObject.isNull("lowestValue") ? null : jsonObject.getDouble("lowestValue");
		this.highestValue = jsonObject.isNull("highestValue") ? null : jsonObject.getDouble("highestValue");
		this.lastValue = jsonObject.isNull("lastValue") ? null : jsonObject.getDouble("lastValue");
		this.volume = jsonObject.isNull("volume") ? null : jsonObject.getDouble("volume");
		this.askedValue = jsonObject.isNull("askedValue") ? null : jsonObject.getDouble("askedValue");
		this.adjclose = jsonObject.isNull("adjclose") ? null : jsonObject.getDouble("adjclose");
		
		this.changeValue = jsonObject.isNull("changeValue") ? "" : jsonObject.getString("changeValue");
		
		this.type = jsonObject.getString("type");
		
		this.retrievedDate = jsonObject.isNull("retrievedDate") ? null :
         	new Date(jsonObject.getLong("retrievedDate"));
		this.creationDate = jsonObject.isNull("creationDate") ? null :
         	new Date(jsonObject.getLong("creationDate"));
	}

	//Getters et setters 
	
	public String getIdRawMarketData() {
		return idRawMarketData;
	}

	public void setIdRawMarketData(String idRawMarketData) {
		this.idRawMarketData = idRawMarketData;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getOpenedValue() {
		return openedValue;
	}

	public void setOpenedValue(Double openedValue) {
		this.openedValue = openedValue;
	}

	public Double getClosedValue() {
		return closedValue;
	}

	public void setClosedValue(Double closedValue) {
		this.closedValue = closedValue;
	}

	public Double getLowestValue() {
		return lowestValue;
	}

	public void setLowestValue(Double lowestValue) {
		this.lowestValue = lowestValue;
	}

	public Double getHighestValue() {
		return highestValue;
	}

	public void setHighestValue(Double highestValue) {
		this.highestValue = highestValue;
	}

	public Double getLastValue() {
		return lastValue;
	}

	public void setLastValue(Double lastValue) {
		this.lastValue = lastValue;
	}

	public Double getVolume() {
		return volume;
	}

	public void setVolume(Double volume) {
		this.volume = volume;
	}

	public Double getAskedValue() {
		return askedValue;
	}

	public void setAskedValue(Double askedValue) {
		this.askedValue = askedValue;
	}

	public Double getAdjclose() {
		return adjclose;
	}

	public void setAdjclose(Double adjclose) {
		this.adjclose = adjclose;
	}

	public String getChangeValue() {
		return changeValue;
	}

	public void setChangeValue(String changeValue) {
		this.changeValue = changeValue;
	}

	public Date getRetrievedDate() {
		return retrievedDate;
	}

	public void setRetrievedDate(Date retrievedDate) {
		this.retrievedDate = retrievedDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getCreationDate() {
		return creationDate;
	}
}
