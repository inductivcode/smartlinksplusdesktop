package com.inductivtechnologies.smartlinksplusDesktop.models.account;

import java.io.Serializable;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.inductivtechnologies.smartlinksplusDesktop.util.data.EntitiesUtil;

/**
 * Token : Pour la gestion de la sécurité 
*/
public class Token implements Serializable {

	public static final String TOKEN_DATA_NAME = "token";

	/**
	 * serialVersionUID Pour la sérialisation 
	 */
	private static final long serialVersionUID = -1908602138658162464L;

	/**
	 * idToken, identifiant unique de l'objet
	 */
	private String idToken;
	
	/**
	 * value, La valeur du token 
	 */
	private String value;
	
	/**
	 * expirationDate : Expiration du token (Après 24H)
	 */
	private Date expirationDate;
	
	/**
	 * creationDate : La date de création du Token
	 */
	private Date creationDate;
	
	/**
	 * Constructeur 
	*/
	public Token() {
	}
	
	/**
	 * Constructeur 
	*/
	public Token(String idToken, String value, Date expirationDate, Date creationDate) {
		super();
		this.idToken = idToken;
		this.value = value;
		this.expirationDate = expirationDate;
		this.creationDate = creationDate;
	}
	
	/**
	 * Constructeur 
	*/
	public Token(JSONObject jsonObject) throws JSONException {
		super();
		 this.idToken = jsonObject.isNull("idToken") ? "" : EntitiesUtil
                 .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idToken"));
         
         this.value = jsonObject.isNull("value") ? "" : jsonObject.getString("value");
         
         this.expirationDate =  jsonObject.isNull("expirationDate") ? null :
         	new Date(jsonObject.getLong("expirationDate"));
         this.creationDate =  jsonObject.isNull("creationDate") ? null :
         	new Date(jsonObject.getLong("creationDate"));
    }

	// Getters et setters

	public String getIdToken() {
		return idToken;
	}

	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}	
}
