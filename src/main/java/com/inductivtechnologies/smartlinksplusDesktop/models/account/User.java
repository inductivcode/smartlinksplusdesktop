package com.inductivtechnologies.smartlinksplusDesktop.models.account;

import java.io.Serializable;

import org.bson.types.ObjectId;

/**
 * Utilisateur 
 */
public class User implements Serializable{

	/**
	 * serialVersionUID identifiant unique pour 
	 * la sérialisation de l'objet, permettant la persistance 
	 * de l'objet.
	 */
	private static final long serialVersionUID = 3621585915696901026L;

	/**
	 * idUser identifiant unique de l'objet, modifiable
	 */
	private ObjectId idUser;
	
	/**
	 * credential : Informationsd'authentification
	 */
	private Credential credential;
	
	/**
	 * token : Le token 
	 */
	private Token token;
	
	/**
	 * Constructeur
	*/
	public User(){
		super();
	}
	
	/**
	 * Constructeur
	*/
	public User(ObjectId idUser, Credential credential, Token token) {
		super();
		this.idUser = idUser;
		this.credential = credential;
		this.token = token;
	}

	// Getters and setters
	
	public void setIdUser(ObjectId idUser) {
		this.idUser = idUser;
	}

	public void setCredentials(Credential credential) {
		this.credential = credential;
	}

	public Credential getCredentials() {
		return credential;
	}

	public ObjectId getIdUser() {
		return idUser;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}
}

